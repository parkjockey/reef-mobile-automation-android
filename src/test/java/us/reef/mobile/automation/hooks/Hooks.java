package us.reef.mobile.automation.hooks;

import io.cucumber.java.Status;
import us.reef.mobile.automation.exceptions.MobileAutomationException;
import us.reef.mobile.automation.rest.proxy.BrowserStackProxy;
import us.reef.mobile.automation.rest.proxy.TestRailProxy;
import us.reef.mobile.automation.ui.base.BaseDriver;
import us.reef.mobile.automation.storage.Storage;
import us.reef.mobile.automation.config.SpringConfig;
import us.reef.mobile.automation.storage.scenario.ScenarioEntity;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = { SpringConfig.class })
public class Hooks {

    @Autowired
    private Storage storage;

    @Autowired
    private BaseDriver baseDriver;

    @Autowired
    private TestRailProxy testRailProxy;

    @Autowired
    private BrowserStackProxy browserStackProxy;

    @Value("${tr.executeRegression}")
    private boolean executeRegression;

    @Value("${grid}")
    private String grid;

    final Logger logger = LoggerFactory.getLogger(Hooks.class);

    @Before(value = "@ui", order = 0)
    public void scenarioStarted(final Scenario scenario) {
        logger.info("SCENARIO: {} started!", scenario.getName());
        final ScenarioEntity testScenario = storage.getScenarioEntity();
        testScenario.setScenarioName(scenario.getName());
        if (scenario.getSourceTagNames().contains("@ui")) {
            baseDriver.initializeWebDriver();
        } else {
            throw new MobileAutomationException("Scenario must contain tag @ui, check your scenario!");
        }
        if (executeRegression) {
            testRailProxy.startTestRun();
            testRailProxy.setTestCaseId(scenario.getName());
        }
    }

    @After(order = 0)
    public void scenarioFinished(final Scenario scenario) {
        logger.info("SCENARIO: '{}' completed with status '{}'", scenario.getName(), scenario.getStatus());
        if (executeRegression) {
            testRailProxy.setTestCaseResultStatus(scenario.getStatus());
        }
        logger.info("Scenario status is {}", scenario.getStatus());
        if (grid.equals("remote") && (scenario.getStatus().equals(Status.FAILED) || scenario.getStatus().equals(Status.PASSED))) {
            browserStackProxy.setTestScenarioStatus(scenario);
        }
        baseDriver.tearDown();
    }
}