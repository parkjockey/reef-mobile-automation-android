package us.reef.mobile.automation.stepdefs.rest;

import groovy.util.logging.Slf4j;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import us.reef.mobile.automation.rest.proxy.NewUserProxy;
import us.reef.mobile.automation.ui.pages.registration.VerifyPhonePage;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class NewUserStepDef {

    @Autowired
    private NewUserProxy newUserProxy;

    @Autowired
    private VerifyPhonePage verifyPhonePage;

    @Given("^Create(?:d|s)? new user in the system$")
    public void registerNewUser() {
        newUserProxy.createNewUser();
        newUserProxy.getToken();
    }

    @When("^(?:Got|Gets)? verification code$")
    public void getVerificationCode() {
        assertThat(verifyPhonePage.isVerificationPageDisplayed()).as("Verification page did not open!").isTrue();
        newUserProxy.getVerificationCode();
    }
}