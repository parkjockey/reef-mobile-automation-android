package us.reef.mobile.automation.stepdefs.ui.parking;

import io.cucumber.java.en.Then;
import org.assertj.core.api.SoftAssertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import us.reef.mobile.automation.storage.Storage;
import us.reef.mobile.automation.storage.user.ParkingEntity;
import us.reef.mobile.automation.ui.pages.parking.ParkingSessionPage;
import us.reef.mobile.automation.utils.CreditCardUtil;
import us.reef.mobile.automation.utils.ParkingTimeUtil;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import static org.assertj.core.api.Assertions.assertThat;

public class ParkingSessionStepDef {

    @Autowired
    private Storage storage;

    @Autowired
    private ParkingSessionPage parkingSessionPage;

    final Logger logger = LoggerFactory.getLogger(ParkingSessionStepDef.class);

    @Then("^Parking session (?:is |will be )?(?:started|displayed)?$")
    public void parkingSessionIsDisplayed() {
        assertThat(parkingSessionPage.isParkingSessionPageDisplayed()).as("Parking session page is not displayed!").isTrue();
        assertThat(parkingSessionPage.isExtendParkingButtonDisplayed()).as("Extend parking button is not displayed!").isTrue();
    }

    @Then("^Session information after buying will be correct$")
    public void sessionInformationWillBeCorrectAfterBuying() {
        final ParkingEntity parkingEntity = storage.getLastParkingEntity();
        final String expectedTotalParkingAmount = parkingEntity.getTotalParkingAmount();
        final String actualTotalParkingAmount = parkingSessionPage.getActualTotalAmount();
        final String expectedLotName = parkingEntity.getLotName();
        final String actualLotName = parkingSessionPage.getActualLotName();
        final String expectedPlate = parkingEntity.getVehicle();
        final String actualPlate = parkingSessionPage.getActualVehicle();
        final String expectedEndTime = ParkingTimeUtil.buyParkingExpectedTime(parkingEntity.getSelectedTime(),
                parkingEntity.getSelectedHours(), parkingEntity.getSelectedMinutes(),
                DateTimeFormatter.ofPattern(storage.getMobileEntity().getTimeFormat()));
        final String expectedParkingEndTimeFirst = LocalTime.of(
                ParkingTimeUtil.extractHours(expectedEndTime),
                ParkingTimeUtil.extractMinutes(expectedEndTime)
        ).format(DateTimeFormatter.ofPattern("h':'mm"));
        final String expectedParkingEndTimeSecond = LocalTime.of(
                ParkingTimeUtil.extractHours(expectedEndTime),
                ParkingTimeUtil.extractMinutes(expectedEndTime)
        ).format(DateTimeFormatter.ofPattern("k':'mm"));

        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(expectedTotalParkingAmount).as("Parking amount is not correct!")
                    .isEqualTo(actualTotalParkingAmount);
            softly.assertThat(expectedLotName).as("Lot name is not correct!")
                    .isEqualTo(actualLotName);
            softly.assertThat(expectedPlate).as("Vehicle plate is not correct!")
                    .isEqualTo(actualPlate);
            final String actualEndTime = parkingSessionPage.getActualEndTime();
            if (actualEndTime.contains("pm") || actualEndTime.contains("am")) {
                softly.assertThat(actualEndTime).as("End time is not correct!")
                        .contains(expectedParkingEndTimeFirst);
            } else {
                softly.assertThat(actualEndTime).as("End time is not correct!")
                        .contains(expectedParkingEndTimeSecond);
            }
        });
        storage.getFirstParking().setPurchaseNumber(parkingSessionPage.getPurchaseNumber());
        logger.info("Total Amount, Lot name, Vehicle and End time are correct.");
    }

    @Then("^Session information after extending will be correct$")
    public void sessionInformationWillBeCorrectAfterExtending() {
        final String actualParkingStartTime = parkingSessionPage.getActualStartTime();
        final String actualParkingEndTime = parkingSessionPage.getActualEndTime();
        final String actualLotName = parkingSessionPage.getActualLotName();
        final String actualVehiclePlate = parkingSessionPage.getActualVehicle();
        final String actualPurchaseNumber = parkingSessionPage.getPurchaseNumber();
        final String actualTotalTime = parkingSessionPage.getActualTotalTime();
        final String actualCreditCard = parkingSessionPage.getActualCreditCard();
        final String actualTotalAmount = parkingSessionPage.getActualTotalAmount();

        final ParkingEntity firstParking = storage.getFirstParking();
        final ParkingEntity secondParking = storage.getLastParkingEntity();
        final String expectedParkingStartTimeFirst = firstParking.getStartTime();
        final String expectedParkingStartTimeSecond = LocalTime.of(
                ParkingTimeUtil.extractHours(firstParking.getStartTime()),
                ParkingTimeUtil.extractMinutes(firstParking.getStartTime())
        ).plusMinutes(1).format(DateTimeFormatter.ofPattern("h':'mm"));
        final String expectedParkingEndTimeFirst = LocalTime.of(
                ParkingTimeUtil.extractHours(secondParking.getEndTime()),
                ParkingTimeUtil.extractMinutes(secondParking.getEndTime())
        ).format(DateTimeFormatter.ofPattern("h':'mm"));
        final String expectedParkingEndTimeSecond = LocalTime.of(
                ParkingTimeUtil.extractHours(secondParking.getEndTime()),
                ParkingTimeUtil.extractMinutes(secondParking.getEndTime())
        ).plusMinutes(1).format(DateTimeFormatter.ofPattern("h':'mm"));
        final String expectedLotName = secondParking.getLotName();
        final String expectedVehiclePlate = secondParking.getVehicle();
        final String expectedPurchaseNumber = firstParking.getPurchaseNumber();
        final int selectedHours = firstParking.getSelectedHours() + secondParking.getSelectedHours();
        final int selectedMinutes = firstParking.getSelectedMinutes() + secondParking.getSelectedMinutes();
        final String expectedTotalTime = selectedHours + "h : " + selectedMinutes + "m";
        final String expectedCreditCard = CreditCardUtil.getCreditCardValueForExtensionParkingPage(
                storage.getLastUserEntity().getCreditCards().get(0));
        logger.info("First parking total price: {}", firstParking.getTotalParkingAmount());
        logger.info("Second parking total price: {}", secondParking.getTotalParkingAmount());
        final double firstTotalAmount = Double.parseDouble(firstParking.getTotalParkingAmount().replaceAll("\\$", ""));
        final double secondTotalAmount = Double.parseDouble(secondParking.getTotalParkingAmount().replaceAll("\\$", ""));
        final String expectedTotalAmount = "$" + (firstTotalAmount + secondTotalAmount);

        SoftAssertions.assertSoftly(softly -> {
            logger.info("Checking parking start time.");
            if (actualParkingStartTime.contains(expectedParkingStartTimeFirst)) {
                softly.assertThat(actualParkingStartTime).as("Start time is not correct!").contains(expectedParkingStartTimeFirst);
            } else {
                softly.assertThat(actualParkingStartTime).as("Start time is not correct!").contains(expectedParkingStartTimeSecond);
            }
            logger.info("Checking parking lot name.");
            if (actualParkingEndTime.contains(expectedParkingEndTimeFirst)) {
                softly.assertThat(actualParkingEndTime).as("End time is not correct!").contains(expectedParkingEndTimeFirst);
            } else {
                softly.assertThat(actualParkingEndTime).as("End time is not correct!").contains(expectedParkingEndTimeSecond);
            }
            logger.info("Checking parking lot name.");
            softly.assertThat(expectedLotName).as("Lot name is not correct!").isEqualTo(actualLotName);
            logger.info("Checking parking vehicle plate.");
            softly.assertThat(expectedVehiclePlate).as("Vehicle plate is not correct!").isEqualTo(actualVehiclePlate);
            logger.info("Checking parking purchase number.");
            softly.assertThat(expectedPurchaseNumber).as("Purchase number is not correct!").isEqualTo(actualPurchaseNumber);
            logger.info("Checking parking total time.");
            softly.assertThat(expectedTotalTime).as("Total time is not correct!").isEqualTo(actualTotalTime);
            logger.info("Checking parking credit card.");
            softly.assertThat(expectedCreditCard).as("Credit card is not correct!").isEqualTo(actualCreditCard);
            logger.info("Checking parking total amount.");
            softly.assertThat(expectedTotalAmount).as("Total amount is not correct!").isEqualTo(actualTotalAmount);
        });
        logger.info("Extended parking information is correct.");
    }

    @Then("^(?:Will )?[Tt]ap(?:s|ped)? extend parking button$")
    public void tapExtendParkingButton() {
        parkingSessionPage.clickExtendParkingButton();
    }
}
