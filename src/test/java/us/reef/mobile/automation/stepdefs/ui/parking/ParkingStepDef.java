package us.reef.mobile.automation.stepdefs.ui.parking;

import io.cucumber.java.en.Then;
import org.assertj.core.api.SoftAssertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.reef.mobile.automation.storage.Storage;
import us.reef.mobile.automation.storage.user.ParkingEntity;
import us.reef.mobile.automation.ui.pages.parking.BuyParkingPage;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import us.reef.mobile.automation.ui.pages.parking.ExtendParkingPage;
import us.reef.mobile.automation.utils.CreditCardUtil;
import us.reef.mobile.automation.utils.ParkingTimeUtil;

import java.time.format.DateTimeFormatter;

import static org.assertj.core.api.Assertions.*;


public class ParkingStepDef {

    @Autowired
    private Storage storage;

    @Autowired
    private BuyParkingPage buyParkingPage;

    @Autowired
    private ExtendParkingPage extendParkingPage;

    final Logger logger = LoggerFactory.getLogger(ParkingStepDef.class);

    @Then("^Will be navigated to Extend Parking page for the same lot$")
    public void willBeNavigatedToExtendParkingPageForSameLot() {
        final String lotName = storage.getFirstParking().getLotName();
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(extendParkingPage.isExtendParkingPageDisplayed())
                    .as("Extend parking page is not displayed!").isTrue();
            softly.assertThat(extendParkingPage.getLotName())
                    .as("Lot name is not correct!").isEqualTo(lotName);
        });
    }

    @When("^(?:Will )?[Ss]elect(?:s|ed)? parking ([Ee]xtension )?duration of (\\d+) hour(?:s)? and (\\d+) minutes$")
    public void selectParkingDuration(final String extension, final int hours, final int minutes) {
        if (extension == null) {
            assertThat(buyParkingPage.isBuyParkingPageDisplayed()).as("Buy parking page is not displayed!").isTrue();
        } else {
            assertThat(extendParkingPage.isExtendParkingPageDisplayed()).as("Extend parking page is not displayed!").isTrue();
        }
        buyParkingPage.clickMinutesWheelButton(minutes);
        buyParkingPage.clickHoursWheelButton(hours);
        if (hours == 0 && minutes == 0) {
            logger.info("Checking is Notification Message shown.");
            if (!buyParkingPage.getMobilePhone().contains("Xiaomi")) {
                assertThat(buyParkingPage.isSnackBarMessageDisplayed())
                        .as("Notification message that period of time needs to be selected is nto displayed!").isTrue();
            }
            assertThat(buyParkingPage.isPleaseTryAgainButtonDisplayed())
                    .as("Please try again button is not displayed.").isTrue();
        } else if (extension == null) {
            buyParkingPage.checksThatBuyEndTimeIsCorrect(hours, minutes);
        } else {
            extendParkingPage.checksThatExtendEndTimeIsCorrect(hours, minutes);
        }
    }

    @When("^Buys parking with duration of (\\d+) hour(?:s)? and (\\d+) minutes$")
    public void buyParkingWithDuration(final int hours, final int minutes) {
        selectParkingDuration(null, hours, minutes);
        requestRate(null);
        buyParking();
    }

    @When("^(On [Ee]xtension )?(?:Will )?[Tt]ap(?:s|ped)? Request Rate button$")
    public void requestRate(final String extension) {
        buyParkingPage.clickGetRatesButton();
        if (extension == null) {
            assertThat(buyParkingPage.isQuoteFeeCorrect()).as("Quote fee text is not correct!").isTrue();
        }
    }

    @When("^Tap(?:s|ed)? Buy Now button and buys the parking session$")
    public void buyParking() {
        buyParkingPage.clickBuyNowButton();
        assertThat(buyParkingPage.isConfirmVehiclePopupDisplayed()).as("Confirm vehicle popup is not displayed").isTrue();
        buyParkingPage.clickYesBuyParkingPopupButton();
    }

    @When("^Buys free parking session$")
    public void buyFreeParking() {
        buyParkingPage.clickGetRatesButton();
        assertThat(buyParkingPage.isFreeParkingMessagedDisplayed()).as("Free parking message is not displayed").isTrue();
        buyParkingPage.clickOkFreeParkingButton();
        buyParkingPage.clickBuyNowButton();
        assertThat(buyParkingPage.isConfirmVehiclePopupDisplayed()).as("Confirm vehicle popup is not displayed").isTrue();
        buyParkingPage.clickYesBuyParkingPopupButton();
    }

    @When("^Tap(?:s|ed)? Buy Now button and changes vehicle to (.*)$")
    public void changeVehicleOnBuying(final String vehicle) {
        buyParkingPage.clickBuyNowButton();
        assertThat(buyParkingPage.isConfirmVehiclePopupDisplayed()).as("Confirm vehicle popup is not displayed").isTrue();
        buyParkingPage.clickChangeVehiclePopupButton();
        buyParkingPage.selectVehicle(vehicle);
    }

    @Then("^Buy [Nn]ow button (?:is|will be)? displayed$")
    public void isBuyNowButtonDisplayed() {
        assertThat(buyParkingPage.isBuyNowButtonDisplayed()).as("Buy Now button is not displayed").isTrue();
    }

    @Then("^Request Rate (?:is|will be)? displayed$")
    public void isRequestRateDisplayed() {
        assertThat(buyParkingPage.isRequestRateButtonDisplayed()).as("Buy Now button is not displayed").isTrue();
    }

    @Then("^Tap(?:s|ped)? Buy Now button$")
    public void clickBuyNowButton() {
        buyParkingPage.clickBuyNowButton();
    }

    @Then("^Information on Extend Parking page will be correct$")
    public void informationOnExtendParkingPageWillBeCorrect() {

        assertThat(extendParkingPage.isExtendParkingPageDisplayed()).as("Extend Parking page is not displayed!").isTrue();

        final ParkingEntity originalParking = storage.getLastParkingEntity();
        final String actualVehiclePlate = extendParkingPage.getVehiclePlate();
        final String actualCreditCard = extendParkingPage.getCreditCard();
        final String actualOriginalSessionTime = extendParkingPage.getOriginalSessionTime();
        final String actualExtendTime = extendParkingPage.getExtendUntilTime();
        final String expectedVehiclePlate = originalParking.getVehicle();
        final String expectedCreditCard = CreditCardUtil.getCreditCardValueForExtensionParkingPage(
                storage.getLastUserEntity().getCreditCards().get(0));
        final String expectedOriginalSelectedTime = originalParking.getSelectedTime();
        final String expectedExtendUntilFirst = ParkingTimeUtil.extendParkingExpectedEndTime(1, 0,0,
                DateTimeFormatter.ofPattern(storage.getMobileEntity().getTimeFormat()));
        final String expectedExtendUntilSecond = ParkingTimeUtil.extendParkingExpectedEndTime(1, 0,0,
                DateTimeFormatter.ofPattern(storage.getMobileEntity().getTimeFormat()));
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(actualVehiclePlate)
                    .as("Vehicle plate is not correct!")
                    .isEqualTo(expectedVehiclePlate);
            softly.assertThat(actualCreditCard)
                    .as("Credit card is not correct!")
                    .isEqualTo(expectedCreditCard);
            softly.assertThat(actualOriginalSessionTime)
                    .as("Original session is not correct!")
                    .isEqualTo(expectedOriginalSelectedTime);
            if (actualExtendTime.contains(expectedExtendUntilFirst)) {
                softly.assertThat(actualExtendTime)
                        .as("Actual extend time is not correct!")
                        .contains(expectedExtendUntilFirst);
            } else {
                softly.assertThat(actualExtendTime)
                        .as("Actual extend time is not correct!")
                        .contains(expectedExtendUntilSecond);
            }
        });
        final ParkingEntity extendParkingEntity = ParkingEntity.builder()
                .vehicle(expectedVehiclePlate)
                .creditCard(expectedCreditCard)
                .endTime(actualExtendTime)
                .build();
        storage.getParkingEntities().add(extendParkingEntity);
        logger.info("Vehicle plate, credit card, Original session time and Extend Until time are correct.");
    }

    @Then("^Extend [Nn]ow button (?:is|will be)? displayed$")
    public void isExtendNowButtonDisplayed() {
        assertThat(extendParkingPage.isExtendNowButtonDisplayed()).as("Extend Now button is not displayed").isTrue();
    }

    @When("^(?:Will )?[Tt]ap(?:s|ped)? Extend Now button$")
    public void extendParking() {
        extendParkingPage.clickExtendNowButton();
    }
}