package us.reef.mobile.automation.stepdefs.ui.user;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.SoftAssertions;
import org.springframework.beans.factory.annotation.Autowired;
import us.reef.mobile.automation.storage.Storage;
import us.reef.mobile.automation.storage.user.UserEntity;
import us.reef.mobile.automation.ui.pages.HomePage;
import us.reef.mobile.automation.ui.pages.map.MapPage;
import us.reef.mobile.automation.ui.pages.registration.ReefInitialPage;
import us.reef.mobile.automation.ui.pages.registration.SignInPage;
import us.reef.mobile.automation.ui.pages.registration.VerifyPhonePage;

import static org.assertj.core.api.Assertions.assertThat;

public class LoginStepDef {

    @Autowired
    private Storage storage;

    @Autowired
    private ReefInitialPage reefInitialPage;

    @Autowired
    private SignInPage signInPage;

    @Autowired
    private VerifyPhonePage verifyPhonePage;

    @Autowired
    private HomePage homePage;

    @Autowired
    private MapPage mapPage;

    @Given("^User tapped on sign up button$")
    public void clickSignUpButton() {
        assertThat(reefInitialPage.isReefInitialPageDisplayed()).as("Reef initial page is not displayed!").isTrue();
        reefInitialPage.clickSignUpButton();
    }

    @Given("^User tapped on login button$")
    public void clickLoginButton() {
        assertThat(reefInitialPage.isReefInitialPageDisplayed()).as("Reef initial page is not displayed!").isTrue();
        reefInitialPage.clickLogInButton();
    }

    @When("^Sign(?:ed|s)? in with valid credentials$")
    public void signInWithValidCredentials() {
        assertThat(signInPage.isSignInPageDisplayed()).as("Sign in page is no displayed!").isTrue();

        final UserEntity user = storage.getLastUserEntity();
        signInPage.enterEmailAndPassword(user.getEmail(), user.getPassword());
        signInPage.clickSignInButton();
    }

    @When("^Enter(?:s|ed)? verification code$")
    public void entersVerificationCode() {
        final UserEntity user = storage.getLastUserEntity();
        verifyPhonePage.enterVerificationCode(user.getVerificationCode());
    }

    @Then("^(?:User )?(?:[Ww]ill be|is)? logged in successfully$")
    public void loggedInSuccessfully() {
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(homePage.isHomePageDisplayed()).as("Home page is not displayed!").isTrue();
            softly.assertThat(homePage.isPayNowSelectedByDefault()).as("Pay Now button is not selected by default!").isTrue();
            softly.assertThat(mapPage.isMapDisplayed()).as("Map is not displayed!").isTrue();
            softly.assertThat(mapPage.isMyLocationAvailable()).as("My Location is not available!").isTrue();
        });
    }
}