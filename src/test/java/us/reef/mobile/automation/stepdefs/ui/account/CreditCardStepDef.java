package us.reef.mobile.automation.stepdefs.ui.account;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import us.reef.mobile.automation.exceptions.MobileAutomationException;
import us.reef.mobile.automation.storage.Storage;
import us.reef.mobile.automation.ui.pages.account.CreditCardPage;
import us.reef.mobile.automation.utils.CreditCardUtil;

import static org.assertj.core.api.Assertions.assertThat;

public class CreditCardStepDef {

    @Autowired
    private Storage storage;

    @Autowired
    private CreditCardPage creditCardPage;

    final Logger logger = LoggerFactory.getLogger(CreditCardStepDef.class);

    @When("^Skips adding credit card$")
    public void skipAddingCreditCard() {
        assertThat(creditCardPage.isCreditCardPageDisplayed()).as("Credit card page is not displayed!").isTrue();
        assertThat(creditCardPage.isCreditCardSectionDisplayed()).as("Credit card section is not displayed!").isTrue();
        creditCardPage.clickOnSkipThisStepButton();
    }

    @When("^On (?:[Ss]ign [Uu]p|[Ss]ign [Ii]n)? [Aa]dd(?:ed|s)? ([Vv]isa|[Mm]aster [Cc]ard|[Aa]merican [Ee]xpress|[Dd]iscover) test credit card$")
    public void addTestCreditCard(final String creditCard) {
        assertThat(creditCardPage.isCreditCardPageDisplayed()).as("Credit card page is not displayed!").isTrue();
        assertThat(creditCardPage.isCreditCardSectionDisplayed()).as("Credit card section is not displayed!").isTrue();
        creditCardPage.addTestCreditCard(creditCard);
    }

    @When("^[Aa]dd(?:ed|s)? ([Vv]isa|[Mm]aster [Cc]ard|[Aa]merican [Ee]xpress|[Dd]iscover) test credit card$")
    public void addTestCreditCardOnAccountScreen(final String creditCard) {
        assertThat(creditCardPage.isAddCreditCardPageDisplayed()).as("Credit card page is not displayed!").isTrue();
        assertThat(creditCardPage.isCreditCardSectionDisplayed()).as("Credit card section is not displayed!").isTrue();
        creditCardPage.addTestCreditCard(creditCard);
    }

    @Given("^Tap(?:s|ed)? on Add credit card button$")
    public void tapsOnAddButton() {
        assertThat(creditCardPage.isViewCreditCardsPageDisplayed()).as("View Credit Cards page is not displayed!").isTrue();
        creditCardPage.clickOnAddCreditCardOnViewCreditCards();
    }

    @Then("^Credit card will be added successfully$")
    public void creditCardWillBeAddedSuccessfully() {
        assertThat(creditCardPage.isCreditCardAddedMessageDisplayed()).as("Message that credit card is added is not displayed!").isTrue();
        logger.info("Message that credit card is added is displayed.");
        assertThat(creditCardPage.isViewCreditCardsPageDisplayed()).as("View Credit Cards page is not displayed!").isTrue();
        final int expectedNumberOfCreditCards = storage.getLastUserEntity().getCreditCards().size();
        final int actualNumberOfCreditCards = creditCardPage.getListOfCreditCards().size();
        assertThat(actualNumberOfCreditCards).as("Amount of credit cards on the page is not correct!").isEqualTo(expectedNumberOfCreditCards);
        logger.info("Number of credit cards in the list is correct.");
        final String lastCreditCard;
        if (expectedNumberOfCreditCards > 1) {
            lastCreditCard = storage.getLastUserEntity().getCreditCards().stream()
                    .reduce((first, second) -> second)
                    .orElseThrow(() -> new MobileAutomationException("Last credit card cannot be found."));
            final String expectedCreditCard = CreditCardUtil.getCreditCardValueForAddedCreditCard(lastCreditCard);
            assertThat(creditCardPage.isCreditCardPresentInTheList(expectedCreditCard))
                    .as("Credit card is not present in the list!").isTrue();
        } else {
            lastCreditCard = storage.getLastUserEntity().getCreditCards().stream()
                    .findFirst()
                    .orElseThrow(() -> new MobileAutomationException("First credit card cannot be found."));
           final String expectedCreditCard = CreditCardUtil.getCreditCardValueForAddedCreditCard(lastCreditCard);
            assertThat(creditCardPage.isCreditCardPresentInTheList(expectedCreditCard))
                    .as("Credit card is not present in the list!").isTrue();
        }
        logger.info("Added credit card is present in the list.");
        // Assert that first credit card is primary
        assertThat(creditCardPage.isFirstCreditCardPrimary()).as("First credit card is not primary!").isTrue();
        logger.info("First credit card is primary.");
    }
}