package us.reef.mobile.automation.stepdefs.ui.paynow;

import io.cucumber.java.en.Then;
import org.assertj.core.api.SoftAssertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import us.reef.mobile.automation.storage.Storage;
import us.reef.mobile.automation.ui.pages.parking.ParkingSessionPage;
import us.reef.mobile.automation.ui.pages.paynow.RecentPage;

public class RecentStepDef {

    @Autowired
    private Storage storage;

    @Autowired
    private RecentPage recentPage;

    @Autowired
    private ParkingSessionPage parkingSessionPage;

    final Logger logger = LoggerFactory.getLogger(RecentStepDef.class);

    @Then("^Recent Lots will be correct$")
    public void recentScreenIsDisplayed() {
        final int numberOfBoughtParking = storage.getParkingEntities().size();
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(recentPage.getNumberOfRecentLots())
                    .as("Amount of recent lots is not correct!")
                    .isEqualTo(numberOfBoughtParking);
            softly.assertThat(recentPage.getNumberOfLotsWithTodayDate("MMM dd"))
                    .as("Last parked dates are not correct!")
                    .isEqualTo(numberOfBoughtParking);
            for (int i = 0; numberOfBoughtParking > i; i++) {
                softly.assertThat(recentPage.isRecentLotWithNameDisplayed(storage.getParkingEntities().get(i).getLotName()))
                        .as("Lot name is not present in Your Recent Lots screen.")
                        .isTrue();
            }
        });
        logger.info("Amount of recent lots is correct.");
        logger.info("Lot names present in Your Recent Lots screen is correct.");
    }

    @Then("^Tap on first lot from the list$")
    public void tapOnFirstLotFromTheList() {
        final String firstLotName = recentPage.getNameOfFirstLotFromTheList();
        recentPage.tapOnFirstLotFromTheList();
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(parkingSessionPage.isParkingSessionPageDisplayed())
                    .as("Parking Session screen is not displayed!")
                    .isTrue();
            softly.assertThat(parkingSessionPage.isParkingLotNameDisplayed(firstLotName))
                    .as("Lot name is not correct!").isTrue();
        });
    }
}
