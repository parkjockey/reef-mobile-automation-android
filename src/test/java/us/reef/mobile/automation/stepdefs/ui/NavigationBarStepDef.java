package us.reef.mobile.automation.stepdefs.ui;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import org.assertj.core.api.SoftAssertions;
import org.springframework.beans.factory.annotation.Autowired;
import us.reef.mobile.automation.exceptions.MobileAutomationException;
import us.reef.mobile.automation.ui.pages.HomePage;
import us.reef.mobile.automation.ui.pages.account.AccountPage;
import us.reef.mobile.automation.ui.pages.parking.BuyParkingPage;
import us.reef.mobile.automation.ui.pages.paynow.RecentPage;
import us.reef.mobile.automation.ui.pages.paynow.TypeNamePage;

import static org.assertj.core.api.Assertions.*;

public class NavigationBarStepDef {

    @Autowired
    private HomePage homePage;

    @Autowired
    private AccountPage accountPage;

    @Autowired
    private TypeNamePage typeNamePage;

    @Autowired
    private BuyParkingPage buyParkingPage;

    @Autowired
    private RecentPage recentPage;

    @Given("^User navigated to account screen$")
    public void navigateToAccountScreen() {
        homePage.clickAccountIcon();
        assertThat(accountPage.isAccountPageDisplayed()).as("Account page is not displayed!").isTrue();
    }

    @Given("^Tap(?:s|ped) on Find Parking button$")
    public void clickOnFindParkingButton() {
        homePage.clickFindParkingButton();
        assertThat(homePage.isNotificationMessageDisplayed()).as("Notification message is not displayed!").isTrue();
    }

    @Given("^Tap(?:s|ped) on Type Name tab$")
    public void clickOnTypeNameTab() {
        homePage.clickTypeNameTab();
        SoftAssertions.assertSoftly(softAssertions -> {
            softAssertions.assertThat(typeNamePage.isTypeNameSearchSectionEmpty())
                    .as("Type name search section is not empty!").isTrue();
            softAssertions.assertThat(typeNamePage.isTypeNameSearchBoxDisplayed())
                    .as("Type name search box is not displayed!").isTrue();
        });
    }

    @When("^Tap(?:s|ped)? [Rr]ecent tab$")
    public void clickRecentTab() {
        homePage.clickRecentTab();
    }

    @When("^(?:Will )?[Tt]ap(?:s|ped)? ([Oo]nce|[Tt]wice) Navigate Back button$")
    public void navigateBackButton(final String taps) {
        if (taps.toLowerCase().contains("once")) {
            buyParkingPage.clickOnNavigateBackButton();
        } else if (taps.toLowerCase().contains("twice")) {
            buyParkingPage.clickOnNavigateBackButton();
            buyParkingPage.clickOnNavigateBackButton();
        } else {
            throw new MobileAutomationException("Number of taps is invalid, use Once or Twice!");
        }
    }

    @When("^Tap(?:s|ped)? [Pp]ay [Nn]ow button$")
    public void clickPayNowButton() {
        assertThat(homePage.isHomePageDisplayed()).as("Home Page is not displayed!").isTrue();
        homePage.clickPayNowButton();
    }

    @When("^Navigate to Your Recent Lots screen$")
    public void navigateToRecentLotsScreen() {
        homePage.waitForSnackBarMessageToDisappear();
        clickPayNowButton();
        clickRecentTab();
        assertThat(recentPage.isRecentPageDisplayed()).as("Recent screen is not displayed!").isTrue();
    }
}