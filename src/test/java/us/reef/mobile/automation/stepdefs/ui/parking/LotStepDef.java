package us.reef.mobile.automation.stepdefs.ui.parking;

import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import us.reef.mobile.automation.ui.pages.HomePage;
import us.reef.mobile.automation.ui.pages.lot.LotPage;

import static org.assertj.core.api.Assertions.assertThat;


public class LotStepDef {

    @Autowired
    private LotPage lotPage;

    @Autowired
    private HomePage homePage;

    @When("^Request(?:s|ed)? [Rr]ates? and proceeds with buying$")
    public void lotWillBeDisplayedAndRequestsRates() {
        assertThat(lotPage.isLotDisplayed()).as("Lot information is not displayed!").isTrue();
        assertThat(lotPage.isGatRatesAndBuyButtonDisplayed()).as("Get Rates and Buy button is displayed!").isTrue();
        lotPage.clickOnGetRatesAndBuyButton();
    }

    @When("^Lot info screen will be displayed$")
    public void isLotDisplayed() {
        assertThat(lotPage.isLotDisplayed()).as("Lot information is not displayed!").isTrue();
    }

    @When("^Navigate from lot info screen to home screen$")
    public void navigateBackToMapScreen() {
        lotPage.clickOnNavigateBackToMapScreenButton();
        assertThat(homePage.isHomePageDisplayed()).as("Home page is not displayed!").isTrue();
    }
}