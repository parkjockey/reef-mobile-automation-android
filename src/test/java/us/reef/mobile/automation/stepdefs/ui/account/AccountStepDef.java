package us.reef.mobile.automation.stepdefs.ui.account;

import io.cucumber.java.en.Given;
import org.springframework.beans.factory.annotation.Autowired;
import us.reef.mobile.automation.ui.pages.account.AccountPage;

public class AccountStepDef {

    @Autowired
    private AccountPage accountPage;

    @Given("^Tap(?:s|ped)? on [Vv]ehicles menu item$")
    public void tapOnVehiclesMenuItem() {
        accountPage.clickOnVehiclesMenuItem();
    }

    @Given("^Tapped on Credit Cards menu item$")
    public void tapOnCreditCardsMenuItem() {
        accountPage.clickOnCreditCardsMenuItem();
    }
    
    @Given("^Tap(?:s|ped)? on Change Password menu item$")
    public void tapOnChangePasswordMenuItem() {
        accountPage.clickOnChangePasswordMenuItem();
    }
}