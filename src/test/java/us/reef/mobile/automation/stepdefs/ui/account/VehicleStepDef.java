package us.reef.mobile.automation.stepdefs.ui.account;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import us.reef.mobile.automation.exceptions.MobileAutomationException;
import us.reef.mobile.automation.storage.Storage;
import us.reef.mobile.automation.ui.pages.account.VehiclesPage;

import static org.assertj.core.api.Assertions.assertThat;

public class VehicleStepDef {

    @Autowired
    private Storage storage;

    @Autowired
    private VehiclesPage vehiclesPage;

    final Logger logger = LoggerFactory.getLogger(VehicleStepDef.class);

    @When("^On (?:[Ss]ign [Uu]p|[Ss]ign [Ii]n)? [Ee]nter(?:s|ed)? license plate (.*) with state (\\w+\\s?-\\s?\\w+)$")
    public void entersLicensePlate(final String licensePlateNumber, final String state) {
        assertThat(vehiclesPage.isLicensePlatePageDisplayed()).as("License plate page is not displayed!").isTrue();
        vehiclesPage.enterLicensePlateNumber(licensePlateNumber);
        vehiclesPage.selectStateFromDropDown(state);
        vehiclesPage.clickOnAddLicensePlateButton();
    }

    @When("^Add(?:s|ed)? new vehicle to account with license plate (.*) and state (\\w+\\s?-\\s?\\w+)$")
    public void addNewVehicleOnAccount(final String licensePlateNumber, final String state) {
        assertThat(vehiclesPage.isAddLicensePlatePageDisplayed()).as("Add License plate page is not displayed!").isTrue();
        vehiclesPage.enterLicensePlateNumber(licensePlateNumber);
        vehiclesPage.selectStateFromDropDown(state);
        vehiclesPage.clickOnAddLicensePlateButton();
    }

    @When("^Skips adding vehicle$")
    public void skipAddingLicensePlate() {
        assertThat(vehiclesPage.isLicensePlatePageDisplayed()).as("License plate page is not displayed!").isTrue();
        vehiclesPage.clickSkipThisStepButton();
    }

    @Given("^Tap(?:s|ed)? on Add vehicle button$")
    public void tapsOnAddButton() {
        assertThat(vehiclesPage.isVehiclesPageDisplayed()).as("Vehicles page is not displayed!").isTrue();
        vehiclesPage.clickAddVehicleButton();
    }

    @Then("^Vehicle will be added successfully$")
    public void vehicleWillBeAdded() {
        assertThat(vehiclesPage.isVehicleAddedMessageDisplayed()).as("Message that vehicle is added is not displayed!").isTrue();
        logger.info("Message that vehicle is added is displayed.");
        assertThat(vehiclesPage.isVehiclesPageDisplayed()).as("Vehicles page is not displayed!").isTrue();
        final int expectedNumberOfVehicles = storage.getLastUserEntity().getLicensePlates().size();
        final int actualNumberOfVehicles = vehiclesPage.getListOfVehicles().size();
        assertThat(actualNumberOfVehicles).as("Amount of vehicles on the page is not correct!").isEqualTo(expectedNumberOfVehicles);
        logger.info("Number of vehicles in the list is correct.");
        final String lastLicensePlate;
        final String lastState;
        if (expectedNumberOfVehicles > 1) {
            lastLicensePlate = storage.getLastUserEntity().getLicensePlates().stream()
                    .reduce((first, second) -> second)
                    .orElseThrow(() -> new MobileAutomationException("Last license plate cannot be found."));
            lastState = storage.getLastUserEntity().getStates().stream()
                    .reduce((first, second) -> second)
                    .orElseThrow(() -> new MobileAutomationException("Last state cannot be found.")).substring(0, 2);
            final String expectedVehicle = lastLicensePlate + " - " + lastState;
            assertThat(vehiclesPage.isVehiclePresentInTheList(expectedVehicle)).as("Vehicle is not present in the list!").isTrue();
        } else {
            lastLicensePlate = storage.getLastUserEntity().getLicensePlates().stream()
                    .findFirst()
                    .orElseThrow(() -> new MobileAutomationException("First license plate cannot be found."));
            lastState = storage.getLastUserEntity().getStates().stream()
                    .findFirst()
                    .orElseThrow(() -> new MobileAutomationException("First state cannot be found.")).substring(0, 2);
            final String expectedVehicle = lastLicensePlate + " - " + lastState;
            assertThat(vehiclesPage.isVehiclePresentInTheList(expectedVehicle)).as("Vehicle is not present in the list!").isTrue();
        }
        logger.info("Added vehicle is present in the list.");
        // Assert that first Vehicle is primary
        assertThat(vehiclesPage.isFirstVehiclePrimary()).as("First vehicle is not primary!").isTrue();
        logger.info("First vehicle is primary.");
    }
}