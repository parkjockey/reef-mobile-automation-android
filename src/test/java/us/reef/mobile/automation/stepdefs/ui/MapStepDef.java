package us.reef.mobile.automation.stepdefs.ui;

import us.reef.mobile.automation.ui.pages.HomePage;
import us.reef.mobile.automation.ui.pages.map.MapPage;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.*;

public class MapStepDef {

    @Autowired
    private HomePage homePage;

    @Autowired
    private MapPage mapPage;

    @When("^Search(?:es|ed)? and selects lot (?:by address|by neighborhood|by city)? (.*)$")
    public void findParkingBySearch(final String lot) {
        homePage.searchForLotAndClickIt(lot);
        assertThat(mapPage.isLotPinDisplayed()).as("Lot Pin is not displayed!").isTrue();
        if (lot.equals("Queen Anne")) {
            assertThat(mapPage.areMultipleLotPinsDisplayed())
                    .as("Multiple lot pins for neighborhood are not displayed").isTrue();
        }
        mapPage.clickOnLotPin();
    }

    @When("^Zooms in (1|2|3) times?$")
    public void zoomIn(final int numberOfTapsOnZoomIn) {
        for (int i = 0; i < numberOfTapsOnZoomIn; i++) {
            mapPage.clickOnZoomInButton();
        }
    }
}