package us.reef.mobile.automation.stepdefs.ui.paynow;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.assertj.core.api.SoftAssertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import us.reef.mobile.automation.ui.pages.parking.BuyParkingPage;
import us.reef.mobile.automation.ui.pages.paynow.TypeNamePage;

import static org.assertj.core.api.Assertions.*;


public class TypeNameStepDef {

    @Autowired
    private TypeNamePage typeNamePage;

    @Autowired
    private BuyParkingPage buyParkingPage;

    final Logger logger = LoggerFactory.getLogger(TypeNameStepDef.class);

    @Given("^(?:Starts typing|Types full name of valid lot)? (.*) in type name search box$")
    public void typeInTypeNameSearchBox(final String lotName) {
        typeNamePage.enterLotNameInTypeNameSearchBox(lotName);
    }

    @Given("^Search results will contain (.*) in lot names$")
    public void searchResultsContainCorrectPartialLotName(final String lotName) {
        assertThat(typeNamePage.doResultsContainsCorrectPartialLotName(lotName))
                .as("Not all search result contain partial lot name {}", lotName).isTrue();
        logger.info("Search results contains partial lot name of {}", lotName);
    }

    @Given("^Select a lot from the list and check that it is displayed in buy parking screen$")
    public void selectFirstLostFromTypeNameListResult() {
        final String firstLotName = typeNamePage.getFirstLotNameFromResultList();
        typeNamePage.clickOnFirstLotFromList();
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(buyParkingPage.isBuyParkingPageDisplayed())
                    .as("Buy parking page is not displayed!").isTrue();
            softly.assertThat(buyParkingPage.areParkingDurationWheelsDisplayed())
                    .as("Parking duration wheels are not displayed!").isTrue();
            softly.assertThat(buyParkingPage.isCorrectLotDisplayed(firstLotName))
                .as("Lot name is not correct!").isTrue();
        });
    }

    @Then("^Will search for lot which does not exist$")
    public void searchLotWhichDoesNotExist() {
        typeNamePage.enterLotNameInTypeNameSearchBox("XXXXXXX");
        assertThat(typeNamePage.isTypeNameSearchSectionEmpty()).as("Search results on Type Name is not empty!").isTrue();
    }
}
