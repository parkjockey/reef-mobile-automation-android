package us.reef.mobile.automation.stepdefs.rest;

import groovy.util.logging.Slf4j;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import us.reef.mobile.automation.rest.proxy.VehicleProxy;

@Slf4j
public class VehicleStepDef {

    @Autowired
    private VehicleProxy vehicleProxy;

    @When("^Add(?:s|ed)? license plate (.*) with state (AL)$")
    public void addedLicensePlate(final String licensePlate, final String state) {
        vehicleProxy.addVehicle(licensePlate, state);
    }
}