Feature: Find and buy parking

  User should be able to search for the lot and buy the parking session.

  It must be possible to change vehicle before buying the parking.
  User is asked to change vehicle or to buy it with current vehicle by clicking on Buy now button.

  Background:
    Given Created new user in the system
    And User tapped on login button
    And Signed in with valid credentials
    And Got verification code
    And Entered verification code
    And On sign in entered license plate PLATE123 with state MN - Minnesota
    And On sign in added Visa test credit card
    And User is logged in successfully

  @ui @critical
  Scenario: User finds short term parking lot and buys it (C13388,C12815)
    Given Searches and selects lot by address 60 Ellis Street Northeast
    When Requests Rates and proceeds with buying
    And Selects parking duration of 1 hour and 15 minutes
    And Taps Request Rate button
    And Taps Buy Now button and buys the parking session
    Then Parking session will be started
    And Session information after buying will be correct
    And Parking receipt email is received with correct content

  @ui @critical @smoke
  Scenario: Buy parking after changing Vehicle from Confirm your Vehicle Pop up (C14820)
    Given User navigated to account screen
    And Tapped on Vehicles menu item
    And Taps on Add vehicle button
    And Adds new vehicle to account with license plate PLATE321 and state AL - Alabama
    And Vehicle will be added successfully
    And Taps twice Navigate Back button
    And Searched and selects lot by address 60 Ellis Street Northeast
    And Requested Rates and proceeds with buying
    And Selected parking duration of 1 hour and 15 minutes
    And Tapped Request Rate button
    When Taps Buy Now button and changes vehicle to PLATE321 - AL
    And Taps Buy Now button and buys the parking session
    Then Parking session will be started
    And Session information after buying will be correct

  @ui @critical
  Scenario: Buy parking during a free period (C15031)
    Given Searches and selects lot by address Nantucket
    When Requests Rates and proceeds with buying
    And Selects parking duration of 1 hour and 0 minutes
    And Buys free parking session
    Then Parking session will be started
    And Session information after buying will be correct