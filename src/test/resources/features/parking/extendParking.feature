Feature: Extend parking

  User should be able to extend current parking session.
  Initial parking start date should not be changed after parking extension.
  Initial parking end date should be updated after parking extension.

  Background:
    Given Created new user in the system
    And User tapped on login button
    And Signed in with valid credentials
    And Got verification code
    And Entered verification code
    And On sign in entered license plate PLATE123 with state MN - Minnesota
    And On sign in added Visa test credit card
    And User is logged in successfully
    And Searches and selects lot by address 60 Ellis Street Northeast
    And Requests Rates and proceeds with buying
    And Selects parking duration of 1 hour and 15 minutes
    And Taps Request Rate button
    And Taps Buy Now button and buys the parking session
    And Parking session started
    And Session information after buying will be correct
    And Tapped extend parking button

  @ui @critical @smoke
  Scenario: User extends short term parking (C13395)
    Given Information on Extend Parking page will be correct
    And Will select parking Extension duration of 0 hours and 0 minutes
    And Will select parking Extension duration of 35 hours and 0 minutes
    And Request Rate will be displayed
    And On Extension Tapped Request Rate button
    And Extend now button will be displayed
    When Taps Extend Now button
    And Parking session is displayed
    Then Session information after extending will be correct
    And Will tap extend parking button
    And Will be navigated to Extend Parking page for the same lot