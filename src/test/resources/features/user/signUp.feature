Feature: Sign Up

  User should be able to register(Sign Up) a new account with:
    - Email(xxxx@testing.reeftechnology.com)
    - Password
    - Mobile Phone(555-xxx-xxxx)

  Also, it should be possible to add Vehicle(License Plate) and Credit Card.

  Possible test Credit Cards to be added:
   - Visa (4111 1111 1111 1111)
   - Master Card (5500 0000 0000 0004)
   - American Express (3400 0000 0000 009)
   - Discover (3643 8999 9600 16)

  @ui @critical
  Scenario: User Signs Up by adding vehicle and credit card (C12572)
    Given User tapped on sign up button
    When Enters valid information in sign up form
    And Agrees to License Agreement
    And Taps on Create Account button
    And Gets verification code
    And Enters verification code
    And On Sign Up Enters license plate PLATE123 with state MN - Minnesota
    And On sign up adds Visa test credit card
    Then Will be logged in successfully

  @ui @api @skip
  Scenario: User adds license plate on BE and adds visa credit card on UI
    Given Creates new user in the system
    When Added license plate Plate-1 with state AL
    And User tapped on login button
    And Signs in with valid credentials
    And Gets verification code
    And Enters verification code
    And On sign in adds Master Card test credit card
    Then Will be logged in successfully