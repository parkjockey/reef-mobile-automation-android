Feature: Sign In

  Existing user should be able to Sign In with hes valid credentials.

  User can skip adding of license plate or credit card.
  User can add license plate or credit card during.

  On successful Sign In:
  - Home landing page should be displayed
  - Pay Now button is selected by default
  - Map should be displayed
  - My Location should be available

  Background:
    Given Create new user in the system

  @ui @critical
  Scenario: Sign in to existing account (C13371)
    Given User tapped on login button
    When Signs in with valid credentials
    And Gets verification code
    And Enters verification code
    And Skips adding vehicle
    And Skips adding credit card
    Then Will be logged in successfully