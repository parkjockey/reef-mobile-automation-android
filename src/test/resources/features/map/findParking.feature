Feature: Find parking

  User should be able to search for lots from find parking section.
  Searching of parking can be performed by typing in: City, neighborhood or address.

  Background:
    Given Created new user in the system
    And User tapped on login button
    And Signed in with valid credentials
    And Got verification code
    And Entered verification code
    And Skips adding vehicle
    And Skips adding credit card
    And User is logged in successfully

  @ui @critical
  Scenario: Search for location (C13381)
    Given Tapped on Find Parking button
    When Searches and selects lot by address 60 Ellis Street Northeast
    And Lot info screen will be displayed
    And Navigate from lot info screen to home screen
    And Taps on Find Parking button
    And Searches and selects lot by neighborhood Queen Anne
    And Lot info screen will be displayed
    And Navigate from lot info screen to home screen
    And Searches and selects lot by city San Diego
    Then Lot info screen will be displayed