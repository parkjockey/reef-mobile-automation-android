Feature: Pay Now

  User should be able to search for lots from Pay Now > Type Name tab.
  The list auto-populates a list of lot options based on what's being entered.
  For example typing "Ma" will bring up lots like MacKenzie Bldg., MacLeod Place, Main and McDermot etc.

  From Pay Now tab user should be able to see Recent Lots and navigate to them.
  If recent lot has an Active session user will be navigated to its parking session screen.
  If recent lot has an expired session user will be navigated to its buy parking screen.

  Background:
    Given Created new user in the system
    And User tapped on login button
    And Signed in with valid credentials
    And Got verification code
    And Entered verification code

  @ui @critical
  Scenario: Type name (C13376)
    Given Skips adding vehicle
    And Skips adding credit card
    And User is logged in successfully
    And Tapped on Type Name tab
    When Starts typing Ma in type name search box
    And Search results will contain Ma in lot names
    And Select a lot from the list and check that it is displayed in buy parking screen
    And Will tap once Navigate Back button
    And Tapped on Type Name tab
    And Types full name of valid lot 60 Ellis St in type name search box
    And Select a lot from the list and check that it is displayed in buy parking screen
    And Will tap once Navigate Back button
    And Tapped on Type Name tab
    Then Will search for lot which does not exist

  @ui @critical
  Scenario: Recent lots (Active session) (C18606)
    Given On sign in entered license plate PLATE123 with state MN - Minnesota
    And On sign in added Visa test credit card
    And User is logged in successfully
    And Searches and selects lot by address 60 Ellis Street Northeast
    And Requests Rates and proceeds with buying
    And Buys parking with duration of 1 hour and 0 minutes
    And Parking session will be started
    And Will tap once Navigate Back button
    And Searches and selects lot by address 811 Peachtree Street Northeast
    And Requests Rates and proceeds with buying
    And Buys parking with duration of 1 hour and 0 minutes
    And Parking session will be started
    And Will tap once Navigate Back button
    When Navigate to Your Recent Lots screen
    Then Recent Lots will be correct
    And Tap on first lot from the list