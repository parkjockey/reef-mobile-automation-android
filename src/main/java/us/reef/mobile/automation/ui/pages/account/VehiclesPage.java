package us.reef.mobile.automation.ui.pages.account;

import io.appium.java_client.android.AndroidElement;
import us.reef.mobile.automation.exceptions.MobileAutomationException;
import us.reef.mobile.automation.storage.Storage;
import us.reef.mobile.automation.ui.base.BaseDriver;
import us.reef.mobile.automation.ui.base.BasePage;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class VehiclesPage extends BasePage {

    @Autowired
    private Storage storage;

    final By signInSignUpLicensePlatePageId = By.id("com.reeftechnology.reef.mobile:id/add_vehicle_title");
    final By accountAddLicensePlatePageId = By.xpath("//*[@text='Add License Plate']");
    final By vehiclesPageId = By.xpath("//*[@text='Vehicles']");
    final By licensePlateNumberField = By.id("com.reeftechnology.reef.mobile:id/license_plate_number");
    final By stateDropDown = By.id("com.reeftechnology.reef.mobile:id/add_vehicle_state_spinner");
    final By addLicensePlateButton = By.id("com.reeftechnology.reef.mobile:id/add_vehicle_button");
    final By skipThisStepButton = By.id("com.reeftechnology.reef.mobile:id/add_vehicle_skip");
    final By vehicleDetails = By.id("com.reeftechnology.reef.mobile:id/view_vehicle_details");
    final By addVehicleButton = By.id("com.reeftechnology.reef.mobile:id/action_add_vehicle");
    final By vehicleAddedMessage = By.id("com.reeftechnology.reef.mobile:id/snackbar_text");

    public VehiclesPage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    public boolean isLicensePlatePageDisplayed() {
        log.info("Checking that License Plate page is displayed.");
        return waitForElementToBeDisplayed(signInSignUpLicensePlatePageId);
    }

    public boolean isAddLicensePlatePageDisplayed() {
        log.info("Checking that Add License Plate page is displayed.");
        return waitForElementToBeDisplayed(accountAddLicensePlatePageId);
    }

    public boolean isVehiclesPageDisplayed() {
        log.info("Checking that Vehicles page is displayed.");
        return waitForElementToBeDisplayed(vehiclesPageId);
    }

    public void enterLicensePlateNumber(final String licensePlateNumber) {
        waitAndSendKeys(licensePlateNumberField, licensePlateNumber);
        if (storage.getLastUserEntity().getLicensePlates() == null) {
            final List<String> licensePlateNumbers = new ArrayList<>();
            licensePlateNumbers.add(licensePlateNumber);
            log.info("Entered license plate number: " + licensePlateNumber);
            storage.getLastUserEntity().setLicensePlates(licensePlateNumbers);
        } else {
            log.info("Entered license plate number: " + licensePlateNumber);
            storage.getLastUserEntity().getLicensePlates().add(licensePlateNumber);
        }
    }

    public void selectStateFromDropDown(final String state) {
        waitAndClick(stateDropDown);
        waitForElementToBeDisplayed(By.className("android.widget.ListView"));
        scrollAndClickOnElementByText(state);
        if (storage.getLastUserEntity().getStates() == null) {
            final List<String> states = new ArrayList<>();
            states.add(state);
            log.info("Selected state: " + state);
            storage.getLastUserEntity().setStates(states);
        } else {
            log.info("Selected state: " + state);
            storage.getLastUserEntity().getStates().add(state);
        }
    }

    public void clickOnAddLicensePlateButton() {
        waitAndClick(addLicensePlateButton);
        log.info("Tapped add license plate button");
    }

    public void clickSkipThisStepButton() {
        waitAndClick(skipThisStepButton);
        log.info("Tapped skip this step button");
    }

    public List<AndroidElement> getListOfVehicles() {
        return driver.findElements(vehicleDetails);
    }

    public void clickAddVehicleButton() {
        waitForElementToBeDisplayed(addVehicleButton);
        waitAndClick(addVehicleButton);
        log.info("Taps on Add vehicle button.");
    }

    public boolean isVehicleAddedMessageDisplayed() {
        return waitForElementToBeDisplayed(vehicleAddedMessage);
    }

    public boolean isVehiclePresentInTheList(final String vehicle) {
        return waitForElementToBeDisplayed(By.xpath("//*[@text='"+vehicle+"']"));
    }

    public boolean isFirstVehiclePrimary() {
        return getListOfVehicles().stream().findFirst()
                .orElseThrow(() -> new MobileAutomationException("First vehicle not found!"))
                .findElement(By.id("com.reeftechnology.reef.mobile:id/view_vehicle_detail"))
                .findElement(By.xpath("//*[@text='Primary']")).isDisplayed();
    }
}