package us.reef.mobile.automation.ui.pages.map;

import us.reef.mobile.automation.ui.base.BaseDriver;
import us.reef.mobile.automation.ui.base.BasePage;
import io.appium.java_client.MobileBy;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class MapPage extends BasePage {

    final By zoomInButton = MobileBy.AccessibilityId("Zoom in");
    final By lotPin = By.xpath("//android.view.View[@content-desc=\"Google Map\"]/android.view.View[1]");
    final By map = By.id("com.reeftechnology.reef.mobile:id/map");
    final By myLocationButton = MobileBy.AccessibilityId("My Location");

    public MapPage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    public void clickOnZoomInButton() {
        driver.hideKeyboard();
        waitAndClick(zoomInButton);
    }

    public boolean isLotPinDisplayed() {
        log.info("Checking is lot pin displayed.");
        return waitForElementToBeDisplayed(lotPin);
    }

    public boolean areMultipleLotPinsDisplayed() {
        log.info("Checking that multiple lot pins are displayed on the map for neighborhood.");
        return driver.findElements(By.xpath("//android.view.View[@content-desc=\"Google Map\"]/android.view.View")).size() > 3;
    }

    public void clickOnLotPin() {
        waitAndClick(lotPin);
        log.info("Tapped on lot pin.");
    }

    public boolean isMapDisplayed() {
        log.info("Checking is Map displayed.");
        return driver.findElement(map).isDisplayed();
    }

    public boolean isMyLocationAvailable() {
        log.info("Checking is My Location available.");
        return driver.findElement(myLocationButton).getAttribute("clickable").equals("true") &&
                driver.findElement(myLocationButton).getAttribute("displayed").equals("true");
    }
}
