package us.reef.mobile.automation.ui.pages.paynow;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import us.reef.mobile.automation.exceptions.MobileAutomationException;
import us.reef.mobile.automation.ui.base.BaseDriver;
import us.reef.mobile.automation.ui.base.BasePage;
import us.reef.mobile.automation.utils.ParkingTimeUtil;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class RecentPage extends BasePage {

    final By recentPageId = By.id("com.reeftechnology.reef.mobile:id/your_lots_label");
    final By recentLotsList = By.xpath("//*[@resource-id='com.reeftechnology.reef.mobile:id/rd_wrapper']");

    public RecentPage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    public boolean isRecentPageDisplayed() {
        log.info("Checking is Recent Lots screen displayed.");
        return waitForElementToBeDisplayed(recentPageId);
    }

    public int getNumberOfRecentLots() {
        return driver.findElements(recentLotsList).size();
    }

    public boolean isRecentLotWithNameDisplayed(final String lotName) {
        log.info("Checking is recent lot with name {} displayed.", lotName);
        return waitForElementToBeDisplayed(By.xpath("//*[@text='"+lotName+"']"));
    }

    public void tapOnFirstLotFromTheList() {
        log.info("Tapped on first lot from the list.");
        driver.findElements(recentLotsList).stream().findFirst()
                .orElseThrow(() -> new MobileAutomationException("First lot from the recent lots is not found!"))
                .click();
    }

    public String getNameOfFirstLotFromTheList() {
        return driver.findElements(recentLotsList).stream().findFirst()
                .orElseThrow(() -> new MobileAutomationException("First lot from the recent lots is not found!"))
                .findElement(By.xpath("//*[@resource-id='com.reeftechnology.reef.mobile:id/list_item_your_lots_lot_name']")).getText();
    }

    public int getNumberOfLotsWithTodayDate(final String expectedFormat) {
        final String expectedDate = ParkingTimeUtil.getLastParkedDate(expectedFormat);
        log.info("Checking is last parked date {}", expectedDate);
        return driver.findElements(By.xpath("//*[starts-with(@text, 'Last parked: "+expectedDate+"')]")).size();
    }
}