package us.reef.mobile.automation.ui.pages;

import us.reef.mobile.automation.exceptions.MobileAutomationException;
import us.reef.mobile.automation.ui.base.BasePage;
import us.reef.mobile.automation.ui.base.BaseDriver;
import io.appium.java_client.android.AndroidElement;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class HomePage extends BasePage {

    final By toolbarPageId = By.id("com.reeftechnology.reef.mobile:id/toolbar");

    final By searchForButton = By.id("com.reeftechnology.reef.mobile:id/action_map_search");
    final By searchBar = By.xpath("//*[@resource-id='com.reeftechnology.reef.mobile:id/places_autocomplete_search_bar']");
    final By searchList = By.id("com.reeftechnology.reef.mobile:id/places_autocomplete_list");
    final By searchListNote20 = By.xpath("//*[@resource-id='com.reeftechnology.reef.mobile:id/places_autocomplete_list']");
    final By payNowButton = By.id("com.reeftechnology.reef.mobile:id/map_payment_button");
    final By findParkingButton = By.id("com.reeftechnology.reef.mobile:id/map_current_location_button");
    final By typeNameTab = By.id("com.reeftechnology.reef.mobile:id/map_payment_enter_name");
    final By recentTab = By.id("com.reeftechnology.reef.mobile:id/map_payment_your_lots");
    final By accountIcon = By.id("com.reeftechnology.reef.mobile:id/action_map_account");
    final By notificationMessage = By.id("com.reeftechnology.reef.mobile:id/snackbar_text");
    final By lotPin = By.className("android.widget.TextView");

    public HomePage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    public boolean isHomePageDisplayed() {
        log.info("Checking is Home Page displayed.");
        return waitForElementToBeDisplayed(toolbarPageId);
    }

    public void searchForLotAndClickIt(final String lot) {
        waitAndClick(searchForButton);
        waitAndSendKeys(searchBar, lot);
        if (getMobilePhone().contains("Samsung Galaxy Note 20")) {
            waitForElementToBeDisplayed(searchListNote20);
        } else {
            waitForElementToBeDisplayed(searchList);
        }
        final List<AndroidElement> listOfLots = driver.findElements(lotPin);
        listOfLots.stream().filter(webElement -> webElement
                .findElement(lotPin).getAttribute("text").equals(lot))
                .findFirst().orElseThrow(() -> new MobileAutomationException("Element with text {} not found!", lot))
                .click();
        log.info("Found lot with name: " + lot);
    }

    public boolean isPayNowSelectedByDefault() {
        log.info("User is logged in successfully.");
        log.info("Checking is Pay Now initially selected on log in.");
        return driver.findElement(payNowButton).getAttribute("clickable").equals("false");
    }

    public void clickPayNowButton() {
        waitForElementToBeDisplayed(payNowButton);
        waitAndClick(payNowButton);
        log.info("Tapped on Pay Now Button.");
    }

    public void clickAccountIcon() {
        waitAndClick(accountIcon);
        log.info("User tapped on account icon.");
    }

    public void clickFindParkingButton() {
        waitAndClick(findParkingButton);
        log.info("User tapped on Find parking button.");
    }

    public void clickTypeNameTab() {
        waitAndClick(typeNameTab);
        log.info("User tapped on Type Name tab.");
    }

    public void clickRecentTab() {
        waitAndClick(recentTab);
        log.info("User tapped on Recent tab.");
    }

    public boolean isNotificationMessageDisplayed() {
        log.info("Checking is notification message displayed.");
        return waitForElementToBeDisplayed(notificationMessage);
    }

    public void waitForSnackBarMessageToDisappear() {
        log.info("Waiting for notification message to disappear.");
        waitForElementNotToBeDisplayed(notificationMessage);
        log.info("Notification message disappeared.");
    }
}