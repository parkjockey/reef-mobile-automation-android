package us.reef.mobile.automation.ui.pages.account;

import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidElement;
import org.springframework.beans.factory.annotation.Autowired;
import us.reef.mobile.automation.exceptions.MobileAutomationException;
import us.reef.mobile.automation.rest.data.creditcard.CreditCard;
import us.reef.mobile.automation.storage.Storage;
import us.reef.mobile.automation.ui.base.BaseDriver;
import us.reef.mobile.automation.ui.base.BasePage;
import lombok.extern.slf4j.Slf4j;
import org.awaitility.Awaitility;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class CreditCardPage extends BasePage {

    @Autowired
    private Storage storage;

    final By creditCardPageId = By.id("com.reeftechnology.reef.mobile:id/add_payment_title");
    final By viewCreditCardsPageId = By.xpath("//*[@text='View Credit Cards']");
    final By addCreditCardPageId = By.xpath("//*[@text='Add Credit Card']");
    final By addCreditCardButtonViewCreditCards = MobileBy.AccessibilityId("Add credit card");
    final By creditCardSection = By.xpath("//*[@class='android.webkit.WebView']");
    final By creditCardNumberField = By.xpath("//*[@resource-id='x_card_num']");
    final By fullCardNameField = By.xpath("//*[@resource-id='exact_cardholder_name']");
    final By cvvField = By.xpath("//*[@resource-id='x_card_code']");
    final By monthSelection = By.xpath("//*[@resource-id='expire_mm']");
    final By yearSelection = By.xpath("//*[@resource-id='expire_yy']");
    final By addCreditCardButton = By.xpath("//*[@class='android.widget.Button']");
    final By skipThisStepButton = By.id("com.reeftechnology.reef.mobile:id/add_payment_skip_link");
    final By creditCardsList = By.xpath("//*[@resource-id='com.reeftechnology.reef.mobile:id/credit_card_list_view']/android.widget.RelativeLayout");
    final By creditCardAddedMessage = By.id("com.reeftechnology.reef.mobile:id/snackbar_text");

    public CreditCardPage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    public boolean isCreditCardPageDisplayed() {
        log.info("Checking is credit card page displayed.");
        return waitForElementToBeDisplayed(creditCardPageId);
    }

    public boolean isViewCreditCardsPageDisplayed() {
        log.info("Checking is view credit cards page displayed.");
        return waitForElementToBeDisplayed(viewCreditCardsPageId);
    }

    public boolean isAddCreditCardPageDisplayed() {
        log.info("Checking is add credit card page displayed.");
        return waitForElementToBeDisplayed(addCreditCardPageId);
    }

    public boolean isCreditCardSectionDisplayed() {
        log.info("Checking is credit card section displayed.");
        Awaitility.await()
                .atMost(60, TimeUnit.SECONDS)
                .pollDelay(5, TimeUnit.SECONDS)
                .pollInterval(5, TimeUnit.SECONDS)
                .until(() -> waitForElementToBeDisplayed(creditCardSection));
        return true;
    }

    public void clickOnSkipThisStepButton() {
        waitAndClick(skipThisStepButton);
        log.info("Clicked on Skip This Step button.");
    }

    public void clickOnAddCreditCardOnViewCreditCards() {
        waitAndClick(addCreditCardButtonViewCreditCards);
    }

    public List<AndroidElement> getListOfCreditCards() {
        return driver.findElements(creditCardsList);
    }

    public boolean isCreditCardAddedMessageDisplayed() {
        return waitForElementToBeDisplayed(creditCardAddedMessage);
    }

    public boolean isCreditCardPresentInTheList(final String creditCard) {
        return waitForElementToBeDisplayed(By.xpath("//*[@text='"+creditCard+"']"));
    }

    public boolean isFirstCreditCardPrimary() {
        return getListOfCreditCards().stream().findFirst()
                .orElseThrow(() -> new MobileAutomationException("First credit card not found!"))
                .findElement(By.xpath("//*[@text='Primary']")).isDisplayed();
    }

    public void addTestCreditCard(final String creditCard) {
        final CreditCard creditCardNumber;
        final String fullCreditCardName = "Automation Test";
        final String cvvNumber = "123";
        switch (creditCard.toUpperCase()) {
            case "VISA":
                creditCardNumber = CreditCard.VISA;
                break;
            case "MASTER CARD":
                creditCardNumber = CreditCard.MASTER_CARD;
                break;
            case "AMERICAN EXPRESS":
                creditCardNumber = CreditCard.AMERICAN_EXPRESS;
                break;
            case "DISCOVER":
                creditCardNumber = CreditCard.DISCOVER;
                break;
            default:
                throw new MobileAutomationException("Credit card {} is not supported!", creditCard);
        }
        waitAndSendKeys(creditCardNumberField, creditCardNumber.card);
        log.info("Entered credit card number: " + creditCardNumber.card);
        waitAndSendKeys(fullCardNameField, fullCreditCardName);
        log.info("Entered card name: " + fullCreditCardName);
        waitAndSendKeys(cvvField, cvvNumber);
        log.info("Entered CVV number: " + cvvNumber);
        waitAndClick(monthSelection);
        waitAndClick(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[2]"));
        log.info("Selected month: 01");
        waitAndClick(yearSelection);
        waitAndClick(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[4]"));
        log.info("Selected year: 2022");
        waitAndClick(addCreditCardButton);
        log.info("Clicked Add Credit Card button.");
        final List<String> creditCardList = new ArrayList<>();
        creditCardList.add(creditCard);
        storage.getLastUserEntity().setCreditCards(creditCardList);
    }
}