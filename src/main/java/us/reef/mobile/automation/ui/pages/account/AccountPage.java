package us.reef.mobile.automation.ui.pages.account;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import us.reef.mobile.automation.ui.base.BaseDriver;
import us.reef.mobile.automation.ui.base.BasePage;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class AccountPage extends BasePage {

    final By accountPageId = By.xpath("//*[@text='Your account']");
    final By vehicles = By.id("com.reeftechnology.reef.mobile:id/account_vehicles");
    final By creditCards = By.id("com.reeftechnology.reef.mobile:id/account_credit_cards");
    final By changePassword = By.id("com.reeftechnology.reef.mobile:id/account_change_password");

    public AccountPage(final BaseDriver baseDriver) {
        super(baseDriver);
    }

    public boolean isAccountPageDisplayed() {
        log.info("Checking is account page displayed.");
        return waitForElementToBeDisplayed(accountPageId);
    }

    public void clickOnVehiclesMenuItem() {
        waitAndClick(vehicles);
        log.info("Tapped on Vehicles menu item.");
    }

    public void clickOnCreditCardsMenuItem() {
        waitAndClick(creditCards);
        log.info("Tapped on Credit Cards menu item.");
    }

    public void clickOnChangePasswordMenuItem() {
        waitAndClick(changePassword);
        log.info("Tapped on Change Password menu item.");
    }
}
