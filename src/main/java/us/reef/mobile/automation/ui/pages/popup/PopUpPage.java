package us.reef.mobile.automation.ui.pages.popup;

import us.reef.mobile.automation.ui.base.BaseDriver;
import us.reef.mobile.automation.ui.base.BasePage;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class PopUpPage extends BasePage {

    final By accessThisDevicePopUpOld = By.id("com.android.packageinstaller:id/permission_message");
    final By accessThisDevicePopUpNew = By.id("com.android.permissioncontroller:id/permission_message");
    final By turnOnBluetoothPopUp = By.id("android:id/message");
    final By allowButton = By.id("com.android.packageinstaller:id/permission_allow_button");
    final By onlyThisTimeButton = By.id("com.android.permissioncontroller:id/permission_allow_one_time_button");
    final By allowBluetoothButton = By.id("android:id/button1");

    public PopUpPage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    public boolean isOldAccessThisDevicePopUpDisplayed() {
        return waitForElementToBeDisplayed(accessThisDevicePopUpOld);
    }

    public boolean isNewAccessThisDevicePopUpDisplayed() {
        return waitForElementToBeDisplayed(accessThisDevicePopUpNew);
    }

    public boolean isTurnOnBluetoothPopUpDisplayed() {
         return waitForElementToBeDisplayed(turnOnBluetoothPopUp);
    }

    public void clickOnAllowButton() {
        waitAndClick(allowButton);
    }

    public void clickOnOnlyThisTimeButton() {
        waitAndClick(onlyThisTimeButton);
    }

    public void clickOnAllowBluetoothButton() {
        waitAndClick(allowBluetoothButton);
    }
}
