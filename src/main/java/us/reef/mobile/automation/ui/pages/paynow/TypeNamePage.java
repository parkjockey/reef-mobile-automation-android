package us.reef.mobile.automation.ui.pages.paynow;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import us.reef.mobile.automation.exceptions.MobileAutomationException;
import us.reef.mobile.automation.ui.base.BaseDriver;
import us.reef.mobile.automation.ui.base.BasePage;

import java.util.ArrayList;
import java.util.List;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class TypeNamePage extends BasePage {

    final By typeNameSearchSection = By.xpath("//*[@resource-id='com.reeftechnology.reef.mobile:id/lot_search_emptyElement']");
    final By searchResults = By.xpath("//*[@resource-id='com.reeftechnology.reef.mobile:id/list_item_lot_search_lot_name']");
    final By typeNameSearchBox = By.id("com.reeftechnology.reef.mobile:id/custombar_text");
    final By firstLotFromResult = By.xpath("//*[@resource-id='com.reeftechnology.reef.mobile:id/lot_search_results_view']/android.widget.RelativeLayout[1]");

    public TypeNamePage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    public boolean isTypeNameSearchSectionEmpty() {
        log.info("Checking that Search section is empty.");
        return waitForElementToBeDisplayed(typeNameSearchSection);
    }

    public boolean isTypeNameSearchBoxDisplayed() {
        log.info("Checking is type name search box displayed.");
        return waitForElementToBeDisplayed(typeNameSearchBox);
    }

    public void enterLotNameInTypeNameSearchBox(final String lotName) {
        waitAndSendKeys(typeNameSearchBox, lotName);
        log.info("Entered partial name of lot name starting with {}", lotName);
    }

    public boolean doResultsContainsCorrectPartialLotName(final String lotName) {
        List<Boolean> listOfResults = new ArrayList<>();
        driver.findElements(searchResults).forEach(androidElement -> {
            listOfResults.add(androidElement.getText().contains(lotName));
        });
        return !listOfResults.contains(false);
    }

    public String getFirstLotNameFromResultList() {
        waitForElementToBeDisplayed(searchResults);
        return driver.findElements(searchResults).stream().findFirst()
                .orElseThrow(() -> new MobileAutomationException("Could not find first element text!"))
                .getText();
    }

    public void clickOnFirstLotFromList() {
        waitForElementToBeDisplayed(firstLotFromResult);
        waitAndClick(firstLotFromResult);
        log.info("Tapped on lot name from result list.");
    }
}
