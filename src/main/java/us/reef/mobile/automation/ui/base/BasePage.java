package us.reef.mobile.automation.ui.base;

import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Value;

@Slf4j
@Getter
public abstract class BasePage {

    @Value("${bs.osVersion}")
    public String osVersion;

    @Value("${bs.mobilePhone}")
    public String mobilePhone;

    protected final AndroidDriver<AndroidElement> driver;
    private final WebDriverWait webDriverWait;

    public BasePage(final BaseDriver baseDriver) {
        this.driver = baseDriver.getDriver();
        this.webDriverWait = new WebDriverWait(driver, 40);
    }

    /**
     * Wait for element to be displayed and send keys to it
     * @param by element on the page
     * @param keysToSend value which we send to the element
     */
    public void waitAndSendKeys(final By by, final String keysToSend) {
        waitForElementToBeDisplayed(by);
        driver.findElement(by).sendKeys(keysToSend);
    }

    /**
     * Wait for element to be displayed and click on it
     * @param by element to be waited for and clicked
     */
    public void waitAndClick(final By by) {
        waitForElementToBeDisplayed(by);
        driver.findElement(by).click();
    }

    /**
     * Wait for element to be displayed
     * @param by element to be waited for
     * @return true if element is displayed otherwise false
     */
    public boolean waitForElementToBeDisplayed(final By by) {
        return getWebDriverWait().until(ExpectedConditions.visibilityOfElementLocated(by)).isDisplayed();
    }

    /**
     * Wait for element not to be displayed
     * @param by element to be waited for
     * @return true if element is not displayed otherwise false
     */
    public boolean waitForElementNotToBeDisplayed(final By by) {
        return getWebDriverWait().until(ExpectedConditions.invisibilityOfElementLocated(by));
    }

    /**
     * Scroll to an element by passed text and click it
     * @param elementText to be scrolled to and clicked on
     */
    public void scrollAndClickOnElementByText(final String elementText) {
        driver.findElement(MobileBy.AndroidUIAutomator(
                "new UiScrollable(new UiSelector()).scrollIntoView(new UiSelector().text(\"" + elementText + "\"));"))
                .click();
    }
}