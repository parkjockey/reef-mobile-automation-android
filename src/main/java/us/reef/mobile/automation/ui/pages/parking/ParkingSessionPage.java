package us.reef.mobile.automation.ui.pages.parking;

import io.appium.java_client.MobileBy;
import us.reef.mobile.automation.ui.base.BaseDriver;
import us.reef.mobile.automation.ui.base.BasePage;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class ParkingSessionPage extends BasePage {

    final By parkingSessionPageId = By.id("com.reeftechnology.reef.mobile:id/parking_session_timer_value");
    final By extendParkingButton = By.id("com.reeftechnology.reef.mobile:id/parking_session_extend_button");
    final By totalAmount = By.id("com.reeftechnology.reef.mobile:id/parking_session_amount_value");
    final By licensePlate = By.id("com.reeftechnology.reef.mobile:id/parking_session_vehicle_value");
    final By lotName = By.id("com.reeftechnology.reef.mobile:id/parking_session_hangTag_name_value");
    final By startTime = By.id("com.reeftechnology.reef.mobile:id/parking_session_start_time_value");
    final By endTime = By.id("com.reeftechnology.reef.mobile:id/parking_session_end_time_value");
    final By purchaseNumber = By.id("com.reeftechnology.reef.mobile:id/parking_session_order_num_value");
    final By totalTime = By.id("com.reeftechnology.reef.mobile:id/parking_session_duration_value");
    final By creditCard = By.id("com.reeftechnology.reef.mobile:id/parking_session_cc_value");

    public ParkingSessionPage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    public boolean isParkingSessionPageDisplayed() {
        log.info("Checking is Parking Session page displayed, session timer must be present.");
        return waitForElementToBeDisplayed(parkingSessionPageId);
    }

    public boolean isExtendParkingButtonDisplayed() {
        log.info("Checking that Extend parking button is displayed.");
        return waitForElementToBeDisplayed(extendParkingButton);
    }

    public boolean isParkingLotNameDisplayed(final String lotName) {
        log.info("Checking is parking lot name displayed and correct.");
        return driver.findElement(this.lotName).getText().equals(lotName);
    }

    public String getActualTotalAmount() {
        waitForElementToBeDisplayed(totalAmount);
        return driver.findElement(totalAmount).getText();
    }

    public String getActualLotName() {
        waitForElementToBeDisplayed(lotName);
        return driver.findElement(lotName).getText();
    }

    public String getActualVehicle() {
        waitForElementToBeDisplayed(licensePlate);
        return driver.findElement(licensePlate).getText();
    }

    public String getActualStartTime() {
        waitForElementToBeDisplayed(startTime);
        return driver.findElement(startTime).getText();
    }

    public String getActualEndTime() {
        waitForElementToBeDisplayed(endTime);
        return driver.findElement(endTime).getText();
    }

    public String getPurchaseNumber() {
        waitForElementToBeDisplayed(purchaseNumber);
        return driver.findElement(purchaseNumber).getText();
    }

    public String getActualTotalTime() {
        waitForElementToBeDisplayed(totalTime);
        return driver.findElement(totalTime).getText();
    }

    public String getActualCreditCard() {
        waitForElementToBeDisplayed(creditCard);
        return driver.findElement(creditCard).getText();
    }

    public void clickExtendParkingButton() {
        log.info("Taps Extend parking button.");
        waitAndClick(extendParkingButton);
    }
}