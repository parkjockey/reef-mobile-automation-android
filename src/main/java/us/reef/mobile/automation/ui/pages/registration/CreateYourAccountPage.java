package us.reef.mobile.automation.ui.pages.registration;

import us.reef.mobile.automation.ui.base.BaseDriver;
import us.reef.mobile.automation.ui.base.BasePage;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class CreateYourAccountPage extends BasePage {

    final By createYourAccountPageId = By.id("com.reeftechnology.reef.mobile:id/text_post_title");
    final By emailField = By.id("com.reeftechnology.reef.mobile:id/email");
    final By passwordField = By.id("com.reeftechnology.reef.mobile:id/password");
    final By mobilePhoneField = By.id("com.reeftechnology.reef.mobile:id/phone");
    final By licenseAgreementCheckbox = By.id("com.reeftechnology.reef.mobile:id/signup_checkbox_tos");
    final By createAccountButton = By.id("com.reeftechnology.reef.mobile:id/sign_in_signup_button");

    public CreateYourAccountPage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    public boolean isCreateYourAccountPageDisplayed() {
        return waitForElementToBeDisplayed(createYourAccountPageId);
    }

    public void enterEmail(final String email) {
        waitAndClick(emailField);
        waitAndSendKeys(emailField, email);
        log.info("Entered email {}", email);
    }

    public void enterPassword(final String password) {
        waitAndClick(passwordField);
        waitAndSendKeys(passwordField, password);
        log.info("Entered password {}", password);
    }

    public void enterMobilePhone(final String mobilePhone) {
        waitAndClick(mobilePhoneField);
        waitAndSendKeys(mobilePhoneField, mobilePhone);
        log.info("Entered mobile phone {}", mobilePhone);
    }

    public void checkLicenseAgreementCheckbox() {
        waitAndClick(licenseAgreementCheckbox);
        log.info("Agrees to License Agreement.");
    }

    public void clickCreateAccountButton() {
        driver.hideKeyboard();
        waitAndClick(createAccountButton);
        log.info("Taps on Create Account button.");
    }
}
