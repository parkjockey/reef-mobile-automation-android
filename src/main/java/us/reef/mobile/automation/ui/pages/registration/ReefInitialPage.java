package us.reef.mobile.automation.ui.pages.registration;

import us.reef.mobile.automation.ui.base.BasePage;
import us.reef.mobile.automation.ui.base.BaseDriver;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class ReefInitialPage extends BasePage {

    final By reefInitialPageId = By.id("com.reeftechnology.reef.mobile:id/signup_splash_container");
    final By signUpButton = By.id("com.reeftechnology.reef.mobile:id/signup_start_signup");
    final By logInButton = By.id("com.reeftechnology.reef.mobile:id/signup_start_login");

    public ReefInitialPage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    public boolean isReefInitialPageDisplayed() {
        return waitForElementToBeDisplayed(reefInitialPageId);
    }

    public void clickSignUpButton() {
        waitAndClick(signUpButton);
        log.info("User tapped sign up button");
    }

    public void clickLogInButton() {
        waitAndClick(logInButton);
        log.info("User tapped log in button");
    }
}