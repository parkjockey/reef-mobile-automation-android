package us.reef.mobile.automation.ui.pages.lot;

import io.appium.java_client.MobileBy;
import us.reef.mobile.automation.ui.base.BaseDriver;
import us.reef.mobile.automation.ui.base.BasePage;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class LotPage extends BasePage {

    final By lotPageId = By.id("com.reeftechnology.reef.mobile:id/view_lot_nestedScrollView");
    final By getRatesButton = By.id("com.reeftechnology.reef.mobile:id/view_lot_buy_button");
    final By navigateBack = MobileBy.AccessibilityId("Navigate up");

    public LotPage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    public boolean isLotDisplayed() {
        log.info("Checking is lot information page displayed.");
        return waitForElementToBeDisplayed(lotPageId);
    }

    public boolean isGatRatesAndBuyButtonDisplayed() {
        log.info("Checking is Get Rates and Buy button displayed.");
        return waitForElementToBeDisplayed(getRatesButton);
    }

    public void clickOnGetRatesAndBuyButton() {
        waitAndClick(getRatesButton);
        log.info("Tapped on Get Rates & Buy button.");
    }

    public void clickOnNavigateBackToMapScreenButton() {
        waitAndClick(navigateBack);
        log.info("Navigated back to home screen.");
    }
}
