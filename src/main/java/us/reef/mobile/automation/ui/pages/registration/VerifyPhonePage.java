package us.reef.mobile.automation.ui.pages.registration;

import us.reef.mobile.automation.ui.base.BasePage;
import us.reef.mobile.automation.ui.base.BaseDriver;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class VerifyPhonePage extends BasePage {

    final By verificationPageId = By.id("com.reeftechnology.reef.mobile:id/verify_phone_explanation");
    final By verificationCodeField = By.id("com.reeftechnology.reef.mobile:id/verify_phone_phone_verification");
    final By verifyPhoneButton = By.id("com.reeftechnology.reef.mobile:id/verify_phone_button");

    public VerifyPhonePage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    public boolean isVerificationPageDisplayed() {
        log.info("Check is Verification page displayed.");
        return waitForElementToBeDisplayed(verificationPageId);
    }

    public void enterVerificationCode(final String verificationCode) {
        waitAndSendKeys(verificationCodeField, verificationCode);
        log.info("Entered verification code: " + verificationCode);
        waitAndClick(verifyPhoneButton);
        log.info("Tapped on Verify Phone button.");
    }
}