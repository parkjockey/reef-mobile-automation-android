package us.reef.mobile.automation.ui.pages.parking;

import lombok.extern.slf4j.Slf4j;
import org.awaitility.Awaitility;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import us.reef.mobile.automation.storage.Storage;
import us.reef.mobile.automation.storage.user.ParkingEntity;
import us.reef.mobile.automation.ui.base.BaseDriver;
import us.reef.mobile.automation.ui.base.BasePage;
import us.reef.mobile.automation.utils.ParkingTimeUtil;

import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class ExtendParkingPage extends BasePage {

    @Autowired
    private Storage storage;

    final By extendParkingPageId = By.xpath("//*[@text='Extend Parking']");
    final By vehiclePlate = By.id("com.reeftechnology.reef.mobile:id/get_quote_license_plate_text");
    final By creditCard = By.id("com.reeftechnology.reef.mobile:id/get_quote_card_extend_text");
    final By originalSessionTime = By.id("com.reeftechnology.reef.mobile:id/get_quote_time_text");
    final By extendUntilTime = By.id("com.reeftechnology.reef.mobile:id/get_quote_extension_end_time_value");
    final By extendNowButton = By.id("com.reeftechnology.reef.mobile:id/get_quote_buy_button");
    final By lotTitleName = By.id("com.reeftechnology.reef.mobile:id/get_quote_how_long_title");

    public ExtendParkingPage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    public boolean isExtendParkingPageDisplayed() {
        log.info("Checking is Extend Parking page displayed.");
        return waitForElementToBeDisplayed(extendParkingPageId);
    }

    public String getVehiclePlate() {
        return driver.findElement(vehiclePlate).getText();
    }

    public String getCreditCard() {
        return driver.findElement(creditCard).getText();
    }

    public String getOriginalSessionTime() {
        return driver.findElement(originalSessionTime).getText();
    }

    public String getExtendUntilTime() {
        return driver.findElement(extendUntilTime).getText();
    }

    public String getLotName () {
        log.info("Checking is correct lot name displayed.");
        return driver.findElement(lotTitleName).getText(); }

    public boolean isExtendNowButtonDisplayed() {
        log.info("Checking is Extend Now button displayed.");
        return waitForElementToBeDisplayed(extendNowButton);
    }

    public void clickExtendNowButton() {
        waitForElementToBeDisplayed(extendNowButton);
        Awaitility.await()
                .atMost(30, TimeUnit.SECONDS)
                .pollDelay(5, TimeUnit.SECONDS)
                .pollInterval(5, TimeUnit.SECONDS)
                .until(() -> !driver.findElement(extendNowButton).getText().isEmpty());
        final String extendNowButtonText = driver.findElement(extendNowButton).getText();
        log.info("Extend now button has text: {}", extendNowButtonText);
        final String totalExtendParkingAmount = extendNowButtonText.replaceAll("\\w+ \\w+: ", "");
        log.info("Extend Amount which user needs to pay is: " + totalExtendParkingAmount);
        setExtendParkingInformation(totalExtendParkingAmount);
        waitAndClick(extendNowButton);
        log.info("Tapped on Extend Now: {} button.", totalExtendParkingAmount);
    }

    private void setExtendParkingInformation(final String totalExtendParkingAmount) {
        final String lotName = driver.findElement(lotTitleName).getText();
        final String vehicle = driver.findElement(vehiclePlate).getText();
        storage.getLastParkingEntity().setLotName(lotName);
        storage.getLastParkingEntity().setVehicle(vehicle);
        storage.getLastParkingEntity().setEndTime(ParkingTimeUtil.getExtendParkingExpectedEndTime());
        storage.getLastParkingEntity().setStartTime(ParkingTimeUtil.getBuyParkingInitialStartTime());
        storage.getLastParkingEntity().setTotalParkingAmount(totalExtendParkingAmount);
    }

    public void checksThatExtendEndTimeIsCorrect(final int hours, final int minutes) {
        if (driver.findElement(originalSessionTime).getText().contains("AM") ||
                driver.findElement(originalSessionTime).getText().contains("PM")) {
            final String format = "h':'mm";
            storage.getMobileEntity().setTimeFormat(format);
            Awaitility.await().atMost(30, TimeUnit.SECONDS)
                    .pollDelay(5, TimeUnit.SECONDS)
                    .pollInterval(5, TimeUnit.SECONDS)
                    .until(() -> driver.findElement(extendUntilTime).getText()
                            .contains(ParkingTimeUtil.extendParkingExpectedEndTime(
                                    hours, minutes, 0, DateTimeFormatter.ofPattern(format)))
                    || driver.findElement(extendUntilTime).getText()
                            .contains(ParkingTimeUtil.extendParkingExpectedEndTime(
                                    hours, minutes, 1, DateTimeFormatter.ofPattern(format))));
        } else {
            final String format = "k':'mm";
            storage.getMobileEntity().setTimeFormat(format);
            Awaitility.await().atMost(30, TimeUnit.SECONDS)
                    .pollDelay(5, TimeUnit.SECONDS)
                    .pollInterval(5, TimeUnit.SECONDS)
                    .until(() -> driver.findElement(extendUntilTime).getText()
                            .contains(ParkingTimeUtil.extendParkingExpectedEndTime(
                                    hours, minutes, 0, DateTimeFormatter.ofPattern(format)))
                    || driver.findElement(extendUntilTime).getText()
                                    .contains(ParkingTimeUtil.extendParkingExpectedEndTime(
                                            hours, minutes, 1, DateTimeFormatter.ofPattern(format))));
        }
        final ParkingEntity parkingEntity = ParkingEntity.builder()
                .selectedHours(hours)
                .selectedMinutes(minutes)
                .build();

        storage.getParkingEntities().add(parkingEntity);
        log.info("End time of parking duration is correct.");
    }
}
