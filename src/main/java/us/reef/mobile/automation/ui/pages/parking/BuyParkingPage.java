package us.reef.mobile.automation.ui.pages.parking;

import io.appium.java_client.MobileBy;
import us.reef.mobile.automation.storage.Storage;
import us.reef.mobile.automation.storage.user.ParkingEntity;
import us.reef.mobile.automation.ui.base.BaseDriver;
import us.reef.mobile.automation.ui.base.BasePage;
import us.reef.mobile.automation.utils.ParkingTimeUtil;
import lombok.extern.slf4j.Slf4j;
import org.awaitility.Awaitility;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class BuyParkingPage extends BasePage {

    @Autowired
    private Storage storage;

    final By buyParkingPageId = By.id("com.reeftechnology.reef.mobile:id/main_container");
    final By durationWheelsContainer = By.xpath("//*[@resource-id='com.reeftechnology.reef.mobile:id/get_quote_time_picker_container']");
    final By hoursWheelButton = By.xpath("*//android.widget.RelativeLayout[1]/android.widget.NumberPicker/android.widget.Button[1]");
    final By hoursWheelValue = By.xpath("*//android.widget.RelativeLayout[1]/android.widget.NumberPicker/android.widget.EditText");
    final By minutesWheelButton = By.xpath("*//android.widget.RelativeLayout[2]/android.widget.NumberPicker/android.widget.Button[1]");
    final By minutesWheelValue = By.xpath("*//android.widget.RelativeLayout[2]/android.widget.NumberPicker/android.widget.EditText");
    final By requestRateButton = By.xpath("//*[@text='Request Rate']");
    final By parkingFeeText = By.id("com.reeftechnology.reef.mobile:id/get_quote_fee");
    final By buyNowButton = By.id("com.reeftechnology.reef.mobile:id/get_quote_buy_button");
    final By pleaseTryAgainButton = By.xpath("//*[@text='Please try again']");
    final By periodOfTimeNotificationMessage = By.id("com.reeftechnology.reef.mobile:id/snackbar_text");
    final By confirmYourVehiclePopUp = By.id("com.reeftechnology.reef.mobile:id/alertTitle");
    final By freeParkingMessage = By.id("android:id/message");
    final By yesBuyParkingPopupButton = By.id("android:id/button1");
    final By changeVehiclePopupButton = By.id("android:id/button2");
    final By selectedZone = By.xpath("//*[@text='Main Entrance']");
    final By selectedTime = By.id("com.reeftechnology.reef.mobile:id/get_quote_time_text");
    final By lotTitleName = By.id("com.reeftechnology.reef.mobile:id/get_quote_how_long_title");
    final By vehicleName = By.xpath("*//android.widget.RelativeLayout[2]/android.widget.Spinner/android.widget.TextView");
    final By navigateBackButton = MobileBy.AccessibilityId("Navigate up");

    public BuyParkingPage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    public boolean isBuyParkingPageDisplayed() {
        log.info("Checking is Buy Parking page displayed.");
        return waitForElementToBeDisplayed(buyParkingPageId);
    }

    public boolean areParkingDurationWheelsDisplayed() {
        log.info("Checking are parking duration wheels displayed.");
        return waitForElementToBeDisplayed(durationWheelsContainer);
    }

    public void clickHoursWheelButton(final int hours) {
        while (Integer.parseInt(driver.findElement(hoursWheelValue).getText().replaceAll(" \\w+", "")) != hours) {
            waitAndClick(hoursWheelButton);
        }
        log.info("User picked {} hours for parking duration.", hours);
    }

    public void clickMinutesWheelButton(final int minutes) {
        while (Integer.parseInt(driver.findElement(minutesWheelValue).getText().replaceAll(" \\w+", "")) != minutes) {
            waitAndClick(minutesWheelButton);
        }
        log.info("User picked {} minutes for parking duration.", minutes);
    }

    public void checksThatBuyEndTimeIsCorrect(final int hours, final int minutes) {
        if (driver.findElement(selectedTime).getText().contains("AM") ||
                driver.findElement(selectedTime).getText().contains("PM")) {
            final String format = "h':'mm";
            storage.getMobileEntity().setTimeFormat(format);
            Awaitility.await().atMost(30, TimeUnit.SECONDS)
                    .pollDelay(5, TimeUnit.SECONDS)
                    .pollInterval(5, TimeUnit.SECONDS)
                    .until(() -> driver.findElement(selectedTime).getText()
                            .contains(ParkingTimeUtil.buyParkingExpectedTime(driver.findElement(selectedTime).getText(),
                                    hours, minutes, DateTimeFormatter.ofPattern(format))));
        } else {
            final String format = "k':'mm";
            storage.getMobileEntity().setTimeFormat(format);
            Awaitility.await().atMost(30, TimeUnit.SECONDS)
                    .pollDelay(5, TimeUnit.SECONDS)
                    .pollInterval(5, TimeUnit.SECONDS)
                    .until(() -> driver.findElement(selectedTime).getText()
                            .contains(ParkingTimeUtil.buyParkingExpectedTime(driver.findElement(selectedTime).getText(),
                                    hours, minutes, DateTimeFormatter.ofPattern(format))));
        }
        final ParkingEntity parkingEntity = ParkingEntity.builder()
                .selectedHours(hours)
                .selectedMinutes(minutes)
                .build();

        storage.getParkingEntities().add(parkingEntity);
        log.info("End time of parking duration is correct.");
    }

    public void clickGetRatesButton() {
        waitAndClick(requestRateButton);
        log.info("Tapped on Get Rates button.");
    }

    public boolean isQuoteFeeCorrect() {
        waitForElementToBeDisplayed(parkingFeeText);
        log.info("Checking is Fee message shown.");
        log.info("Fee message is shown with text: " + driver.findElement(parkingFeeText).getText());
        return driver.findElement(parkingFeeText).getText().contains("Convenience Fee");
    }

    public boolean isBuyNowButtonDisplayed() {
        log.info("Checking is Buy Now button displayed.");
        return waitForElementToBeDisplayed(buyNowButton);
    }

    public boolean isRequestRateButtonDisplayed() {
        log.info("Checking is Request Rate button displayed.");
        return waitForElementToBeDisplayed(requestRateButton);
    }

    public void clickBuyNowButton() {
        waitForElementToBeDisplayed(buyNowButton);
        final String buyNowButtonText = driver.findElement(buyNowButton).getText();
        final String totalParkingAmount = buyNowButtonText.replaceAll("\\w+ \\w+: ", "");
        log.info("Amount which user needs to pay is: " + totalParkingAmount);
        setParkingInformation(totalParkingAmount);
        waitAndClick(buyNowButton);
        log.info("Tapped on Buy Now: {} button.", totalParkingAmount);
    }

    public boolean isConfirmVehiclePopupDisplayed() {
        log.info("Checking is Confirm Vehicle pop up displayed.");
        return waitForElementToBeDisplayed(confirmYourVehiclePopUp);
    }

    public boolean isFreeParkingMessagedDisplayed() {
        log.info("Checking is free message displayed.");
        return waitForElementToBeDisplayed(freeParkingMessage);
    }

    public void clickYesBuyParkingPopupButton() {
        log.info("Clicked Yes Buy parking button on pop up.");
        waitAndClick(yesBuyParkingPopupButton);
    }

    public void clickOkFreeParkingButton() {
        log.info("Clicked Ok button on free parking message.");
        waitAndClick(yesBuyParkingPopupButton);
    }

    public void clickChangeVehiclePopupButton() {
        log.info("Clicked change vehicle button on pop up.");
        waitAndClick(changeVehiclePopupButton);
    }

    public void selectVehicle(final String vehicle) {
        log.info("Changing vehicle to {}", vehicle);
        waitAndClick(By.xpath("//*[@text='"+vehicle+"']"));
    }

    public boolean isPleaseTryAgainButtonDisplayed() {
        log.info("Checking is Please Try Again button displayed.");
        return waitForElementToBeDisplayed(pleaseTryAgainButton);
    }

    public boolean isSnackBarMessageDisplayed () {
        log.info("Checking is notification message(You must select a period of time) displayed.");
        return waitForElementToBeDisplayed(periodOfTimeNotificationMessage);
    }

    public void clickOnNavigateBackButton() {
        waitAndClick(navigateBackButton);
    }

    public boolean isCorrectLotDisplayed(final String lotName) {
        log.info("Checking is lot name displayed on Buy Parking screen.");
        final String actualLotName = driver.findElement(lotTitleName).getText().trim();
        return actualLotName.equals(lotName);
    }

    private void setParkingInformation(final String totalParkingAmount) {
        final String parkingFee;
        if (!totalParkingAmount.equals("$0.00")) {
             parkingFee = driver.findElement(parkingFeeText).getText()
                .replaceAll("[A-z]\\S*", "").trim();
        } else {
             parkingFee = "$0.00";
        }
        final String zone = driver.findElement(selectedZone).getText();
        final String lotName = driver.findElement(lotTitleName).getText().trim();
        final String vehicle = driver.findElement(vehicleName).getText();
        final String fullSelectedTime = driver.findElement(selectedTime).getText();
        storage.getLastParkingEntity().setLotName(lotName);
        storage.getLastParkingEntity().setVehicle(vehicle);
        storage.getLastParkingEntity().setParkingFee(parkingFee);
        storage.getLastParkingEntity().setTotalParkingAmount(totalParkingAmount);
        storage.getLastParkingEntity().setZone(zone);
        storage.getLastParkingEntity().setSelectedTime(fullSelectedTime);
        storage.getLastParkingEntity().setStartTime(ParkingTimeUtil.getBuyParkingInitialStartTime());
        storage.getLastParkingEntity().setEndTime(ParkingTimeUtil.getBuyParkingExpectedEndTime());
    }
}