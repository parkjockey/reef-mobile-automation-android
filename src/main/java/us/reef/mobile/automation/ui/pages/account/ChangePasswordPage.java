package us.reef.mobile.automation.ui.pages.account;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import us.reef.mobile.automation.ui.base.BaseDriver;
import us.reef.mobile.automation.ui.base.BasePage;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 25/11/2020
 */
@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class ChangePasswordPage extends BasePage {

    final By changePasswordPageId = By.id("com.reeftechnology.reef.mobile:id/login_change_password_explanation");

    public ChangePasswordPage(final BaseDriver baseDriver) {
        super(baseDriver);
    }

    public boolean isChangePasswordPageDisplayed() {
        return waitForElementToBeDisplayed(changePasswordPageId);
    }
}
