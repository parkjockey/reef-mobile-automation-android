package us.reef.mobile.automation.ui.pages.registration;

import us.reef.mobile.automation.ui.base.BasePage;
import us.reef.mobile.automation.ui.base.BaseDriver;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class SignInPage extends BasePage {

    final By signInPageId = By.id("com.reeftechnology.reef.mobile:id/login_title");
    final By emailField = By.id("com.reeftechnology.reef.mobile:id/email");
    final By passwordField = By.id("com.reeftechnology.reef.mobile:id/password");
    final By signInButton = By.id("com.reeftechnology.reef.mobile:id/login_button");

    public SignInPage(BaseDriver baseDriver) {
        super(baseDriver);
    }

    public boolean isSignInPageDisplayed() {
        log.info("Check is Sing Un page displayed.");
        return waitForElementToBeDisplayed(signInPageId);
    }

    public void enterEmailAndPassword(final String email, final String password) {
        waitAndSendKeys(emailField, email);
        waitAndSendKeys(passwordField, password);
        log.info("User entered email: {} and password: {}.", email, password);
    }

    public void clickSignInButton() {
        waitAndClick(signInButton);
        log.info("User tapped sign in button.");
    }
}