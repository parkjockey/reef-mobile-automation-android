package us.reef.mobile.automation.storage.user;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder(toBuilder = true)
public class ParkingEntity {
    private String lotName;
    private String parkingFee;
    private String totalParkingAmount;
    private String purchaseNumber;
    private String zone;
    private String selectedTime;
    private String startTime;
    private String endTime;
    private int selectedHours;
    private int selectedMinutes;
    private String vehicle;
    private String creditCard;
    private String hours;
    private String minutes;
}
