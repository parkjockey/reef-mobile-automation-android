package us.reef.mobile.automation.utils;

import lombok.experimental.UtilityClass;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 23/11/2020
 */
@UtilityClass
public class EmailUtil {
    /**
     * Remove initial 0 from time hours.
     *
     * @param time time which needs removing of initial 0 from hours
     * @return time without initial 0 in hours
     */
    public String removeInitialZero(final String time) {
        if (time.substring(0, 1).contains("0")) {
            return time.replaceFirst("0", "");
        } else {
            return time;
        }
    }
}
