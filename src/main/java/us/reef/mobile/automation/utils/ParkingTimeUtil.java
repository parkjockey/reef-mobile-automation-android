package us.reef.mobile.automation.utils;

import lombok.Getter;
import lombok.experimental.UtilityClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

@UtilityClass
public class ParkingTimeUtil {
    @Getter
    private String buyParkingExpectedEndTime;
    @Getter
    private String buyParkingInitialStartTime;
    @Getter
    private String extendParkingExpectedEndTime;

    final Logger logger = LoggerFactory.getLogger(ParkingTimeUtil.class);

    /**
     * Gets the expected buy parking end time.
     *
     * @param time    actual time what user sees(we use start time)
     * @param hours   selected hours
     * @param minutes selected minutes
     * @param format  format which we want and expect {@link DateTimeFormatter}
     * @return expected end time for buy parking
     */
    public String buyParkingExpectedTime(final String time, final int hours, final int minutes, final DateTimeFormatter format) {
        final int firstHours;
        if (time.substring(0, 2).contains(":")) {
            firstHours = Integer.parseInt(time.substring(0, 1));
            buyParkingInitialStartTime = time.substring(0, 4);
        } else if (time.substring(0, 2).endsWith("0")) {
            firstHours = Integer.parseInt(time.substring(0, 2));
            buyParkingInitialStartTime = time.substring(0, 5);
        } else {
            firstHours = Integer.parseInt(time.substring(0, 2).replace("0", ""));
            buyParkingInitialStartTime = time.substring(0, 5);
        }
        final int mins;
        if (time.substring(0, 2).contains(":")) {
            mins = Integer.parseInt(time.substring(2, 4));
        } else {
            mins = Integer.parseInt(time.substring(3, 5));
        }
        buyParkingExpectedEndTime = LocalTime.of(firstHours, mins)
                .plusHours(hours).plusMinutes(minutes)
                .format(format);
        logger.info("Expected buy parking end time: {}", buyParkingExpectedEndTime);
        return buyParkingExpectedEndTime;
    }

    /**
     * Get the expected extend parking end time.
     *
     * @param hours         selected hours which user wants to extend parking duration
     * @param minutes       selected minutes which user wants to extend parking duration
     * @param minutesOffset offset which we use to make a tolerance (usually 1minute max)
     * @param format        format which we want and expect {@link DateTimeFormatter}
     * @return expected end time for extended parking
     */
    public String extendParkingExpectedEndTime(final int hours, final int minutes, final int minutesOffset, final DateTimeFormatter format) {

        final int firstHours;
        final int totalMinutes = minutes + minutesOffset;
        if (buyParkingExpectedEndTime.substring(0, 2).contains(":")) {
            firstHours = Integer.parseInt(buyParkingExpectedEndTime.substring(0, 1));
        } else if (buyParkingExpectedEndTime.substring(0, 2).endsWith("0")) {
            firstHours = Integer.parseInt(buyParkingExpectedEndTime.substring(0, 2));
        } else {
            firstHours = Integer.parseInt(buyParkingExpectedEndTime.substring(0, 2).replace("0", ""));
        }
        final int mins;
        if (buyParkingExpectedEndTime.substring(0, 2).contains(":")) {
            mins = Integer.parseInt(buyParkingExpectedEndTime.substring(2, 4));
        } else {
            mins = Integer.parseInt(buyParkingExpectedEndTime.substring(3, 5));
        }
        extendParkingExpectedEndTime = LocalTime.of(firstHours, mins)
                .plusHours(hours).plusMinutes(totalMinutes)
                .format(format);
        logger.info("Expected extend parking end time {}", extendParkingExpectedEndTime);
        return extendParkingExpectedEndTime;
    }

    /**
     * Extract hours from passed time for example extract hours from 01:15
     * It will return 1hour
     *
     * @param time time from which we want to extract hours
     * @return hours {@link Integer}
     */
    public int extractHours(final String time) {
        if (time.substring(0, 2).contains(":")) {
            return Integer.parseInt(time.substring(0, 1));
        } else if (time.substring(0, 2).endsWith("0")) {
            return Integer.parseInt(time.substring(0, 2));
        } else {
            return Integer.parseInt(time.substring(0, 2).replace("0", ""));
        }
    }

    /**
     * Extract minutes from passed time for example extract minutes from 01:15
     * It will return 15 minutes
     *
     * @param time time from which we want to extract minutes
     * @return minutes {@link Integer}
     */
    public int extractMinutes(final String time) {
        if (time.substring(0, 2).contains(":")) {
            return Integer.parseInt(time.substring(2, 4));
        } else {
            return Integer.parseInt(time.substring(3, 5));
        }
    }

    /**
     * Get Last Parked date, for checking bought parking for today.
     *
     * @return parsed last parked date {@link String}
     */
    public String getLastParkedDate(final String format) {
        return LocalDate.now().format(DateTimeFormatter.ofPattern(format));
    }
}