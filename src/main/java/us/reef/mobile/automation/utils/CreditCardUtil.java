package us.reef.mobile.automation.utils;

import lombok.experimental.UtilityClass;
import us.reef.mobile.automation.exceptions.MobileAutomationException;

@UtilityClass
public class CreditCardUtil {

    private static final String VISA = "VISA";
    private static final String MASTER_CARD = "MASTER CARD";
    private static final String AMERICAN_EXPRESS = "AMERICAN EXPRESS";
    private static final String DISCOVER = "DISCOVER";

    /**
     * Get Credit Card value for Extension Parking screen.
     *
     * @param creditCardName name of credit card
     * @return credit card name with number and expiration date
     */
    public String getCreditCardValueForExtensionParkingPage(final String creditCardName) {
        switch (creditCardName.toUpperCase()) {
            case VISA:
                return "VISA  * 1111";
            case MASTER_CARD:
                return "MASTERCARD  * 0004";
            case AMERICAN_EXPRESS:
                return "AMERICAN EXPRESS  * 0009";
            case DISCOVER:
                return "DINERS CLUB  * 0016";
            default:
                throw new MobileAutomationException("Credit card {} is not supported!", creditCardName);
        }
    }

    /**
     * Get Credit Card value for added credit card.
     *
     * @param creditCardName name of credit card
     * @return credit card value of hidden card number
     */
    public String getCreditCardValueForAddedCreditCard(final String creditCardName) {
        switch (creditCardName.toUpperCase()) {
            case VISA:
                return "• • • • 1111";
            case MASTER_CARD:
                return "• • • • 0004";
            case AMERICAN_EXPRESS:
                return "• • • • 0009";
            case DISCOVER:
                return "• • • • 0016";
            default:
                throw new MobileAutomationException("Credit card {} is not supported!", creditCardName);
        }
    }
}
