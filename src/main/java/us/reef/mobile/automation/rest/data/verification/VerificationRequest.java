package us.reef.mobile.automation.rest.data.verification;

import us.reef.mobile.automation.rest.data.AppVersion;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class VerificationRequest {
    private String emailAddress;
    private String password;
    private String verificationCode;
    private AppVersion appVersion;
}
