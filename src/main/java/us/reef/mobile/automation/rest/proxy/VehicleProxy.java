package us.reef.mobile.automation.rest.proxy;

import us.reef.mobile.automation.exceptions.MobileAutomationException;
import us.reef.mobile.automation.rest.client.BaseRestClient;
import us.reef.mobile.automation.rest.data.AppVersion;
import us.reef.mobile.automation.rest.data.vehicle.VehicleRequest;
import us.reef.mobile.automation.storage.Storage;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class VehicleProxy {

    @Autowired
    private Storage storage;

    @Value("${reef-mobile.url}")
    private String baseUrl;

    /**
     * Add new vehicle
     *
     * @param licensePlate license plate
     * @param stateProvince state province
     */
    public void addVehicle(final String licensePlate, final String stateProvince) {

        final BaseRestClient baseRestClient = new BaseRestClient(baseUrl, storage.getLastUserEntity().getToken());

        final VehicleRequest vehicleRequest = VehicleRequest.builder()
                .action(0)
                .licensePlate(licensePlate)
                .stateProvince(stateProvince)
                .appVersion(new AppVersion())
                .build();

        final Response response = baseRestClient.post(vehicleRequest, "hangTagAccounts/SetVehicle");

        if (response.getStatusCode() != HttpStatus.SC_OK) {
            throw new MobileAutomationException("Vehicle not added! Expected status {} but was {}, Body: {}", HttpStatus.SC_OK, response.getStatusCode(), response.getBody().prettyPrint());
        }

        final List<String> licensePlateNumbers = new ArrayList<>();
        licensePlateNumbers.add(licensePlate);
        final List<String> states = new ArrayList<>();
        states.add(stateProvince);
        storage.getLastUserEntity().setLicensePlates(licensePlateNumbers);
        storage.getLastUserEntity().setStates(states);
    }
}
