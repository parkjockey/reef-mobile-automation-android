package us.reef.mobile.automation.rest.data.creditcard;

public enum CreditCard {

    VISA("4111111111111111"),
    MASTER_CARD("5500000000000004"),
    AMERICAN_EXPRESS("340000000000009"),
    DISCOVER("36438999960016");

    public final String card;

    CreditCard(final String creditCard) {
        this.card = creditCard;
    }
}
