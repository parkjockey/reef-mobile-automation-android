package us.reef.mobile.automation.rest.data;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AppVersion {
    @Builder.Default
    private int appMajorVersion = 100;
    @Builder.Default
    private int appMinorVersion = 1;
    @Builder.Default
    private int appType = 0;
    @Builder.Default
    private int appUTCOffSet = 2;
    @Builder.Default
    private int languageCode = 0;
    @Builder.Default
    private int operatingSystem = 0;
}
